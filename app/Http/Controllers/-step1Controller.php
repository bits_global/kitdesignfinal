<?php

namespace App\Http\Controllers;

/*use App\Http\Requests\CreateHomepageBannersRequest;
use App\Http\Requests\UpdateHomepageBannersRequest;
use App\Repositories\HomepageBannersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
*/

use Illuminate\Support\Facades\Session;
use PDF;
use App\Design;
use App\Font;
use App\Models\DesignLayers;
use App\Models\Clothes;
use App\Models\Collars;
use App\Models\Alphabets;
use App\Models\Symbols;
use App\Models\EditableLogos;
use App\Models\Color;
use App\Embellishment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Cart;
use App\Mail\SendMail; 



class step1Controller extends AppBaseController
{
    /** @var  PrintsRepository */
    //private $homepageBannersRepository;
    /*
    public function __construct()
    {
        //$this->homepageBannersRepository = $homepageBannersRepo;
        //$this->middleware('auth');
    }
*/
    /**
     * Display a listing of the Prints.
     *
     * @param Request $request
     * @return Response
     */
   
    public function index()
    {
        $productID = $_REQUEST['designID'];
        $productName = $_REQUEST['productName'];
        $embs = '';
        if (isset($_REQUEST['emb'])) {
            $embs = Embellishment::where('id', $_REQUEST['emb'])->get();
        }
        $colors = Color::all();
        $fonts = Font::all();
        $alphabets = Alphabets::all();
        $symbols = Symbols::all();
        $elogos = EditableLogos::all();
        $products = Design::where('id', $productID)->get();
        $cloth = Clothes::where('id', $products[0]->prod_id)->get();
        
		
		$collars = Collars::where('category', $products[0]->prod_id)->get();
        $collar = count($collars);
        $layers = DesignLayers::where([['design_id', '=', $productID], ['layer_title', '!=', '']])->get();
        return view('step1-addColor', compact('colors', 'cloth', 'products', 'alphabets', 'symbols', 'elogos', 'fonts', 'collar', 'collars', 'embs', 'layers','productName'));
    }

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.png';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            rename($location . $name, public_path() . '/img/custom_img/' . $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }
    }

    public function downloadPDF(Request $request)
    {
        $data = Session::get('userInfo');

        $input = $request->all();
        $product = Design::where('id', $input['svgData'])->get();
        $cloth = Clothes::where('id', $product[0]->prod_id)->get();

        if($input['svgDataStream']!='' && $product[0]->image!= $input['svgDataStream'])
        {
            $product[0]->image_enc = base64_encode( $input['svgDataStream'] );
        }
        else
        {
            $product[0]->image_enc = base64_encode( $product[0]->image );
        }


        $pdf = PDF::loadView('invoice', compact('input', 'product','cloth', 'data'))->setPaper('a4', 'landscape')->setWarnings(false);
        $time = time();
        $pdf->save(public_path() . '/cartorder/' . $time . '.pdf');
        $product[0]->stop=true;

        $PDF = $pdf->download('invoice.pdf');
//        return $pdf->stream('invoice.pdf');

        if ($PDF)
        {
            return $PDF;
        }
//        $pdf = PDF::loadView('pdfView', compact('input', 'product','cloth', 'data'))->setPaper('a4', 'landscape')->setWarnings(false);
//        return $pdf->stream('invoice.pdf');

        exit;
//        $data = array(
//            'name' => $input['name'],
//            'last_name' => $input['last_name'],
//            'email' => $input['email'],
//            'phone' => $input['phone'],
//            'organization' => $input['organization'],
//            'qty' => $input['qty'],
//            'country' => $input['country'],
//            'city' => $input['city'],
//            'state' => $input['state'],
//            'postalcode' => $input['state'],
//            'comments' => $input['comments'],
//            'PDF_Link' => url('/cartorder') .'/'. $time . '.pdf',
//        );
//        Mail::to([$input['email']])->send(new SendMail($data));
//
//        $product = $product[0];
//        $adding = Cart::add($product->id, $product->name, $input['qty'], $product->price, 0, ['extradata' => $input]);
//
//        session()->flash('message', "Added To Cart ");
//        session()->flash('linkdown', $time);
//        return redirect()->back();


    }

    public function cart()
    {
        return view('cartView');
    }

    public function cart_update(Request $request, $rowId)
    {
        Cart::update($rowId, $request->qty + 1);
        session()->flash('message', "Product quantity has been increased");
        return back();
    }

    public function cart_decrease(Request $request, $rowId)
    {
        //echo $rowId;
        //dd($request->qty);exit;
        Cart::update($rowId, $request->qty - 1);
        session()->flash('message', "Product quantity has been decreased");
        return back();
    }

    public function remove(Request $request, $rowId)
    {
        Cart::remove($rowId);
        session()->flash('message', "Product has been removed");
        return back();
    }

    public function paypal(Request $request)
    {
        Cart::destroy();
        $input = $request->all();
        $ammounting = base64_decode($input['codeview']);

        $b = str_replace( ',', '', $ammounting );

        if( is_numeric( $b ) ) {
            $ammounting = $b;
        }



        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AZp8cUPZ4Xg1CiWU6Z2MCUHoLw8YePCiLId7gf39j9b49bvUzVPW10IqgGymFDJQ6nnIu-5x4xfw_z2h',     // ClientID
                'ELzSHaK4cAnYplshNQ5dgN3dWgu6QcLU8Z7Zxi2gTjbEG7AAKUC--B8nWl9kwKGIWmkvVqhXFFsr_YOi'      // ClientSecret
            )
        );

        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal($ammounting);
        $amount->setCurrency('USD');

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount);
        $homeurl = url('/paypal');

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl($homeurl)
            ->setCancelUrl($homeurl.'?cancelurl');

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);
        try {
            $payment->create($apiContext);

            $papypallobj = [
                "approvallink" => $payment->getApprovalLink(),
                "intent" => $payment->getIntent(),

            ];
            //echo "<a href='".$papypallobj['approvallink']."'>PAY NOW</a>";
           //echo $papypallobj['approvallink'];exit;
        return redirect($papypallobj['approvallink']);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            echo $ex->getData();
        }
    }

    public function sucess()
    {
        return view('succesView');
    }
}