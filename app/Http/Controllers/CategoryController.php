<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imageName = '';
        if( $request->hasFile('image') ) {


            $image = $request->file('image');
            $valid_extensions = ['svg','png'];

            if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .svg, and .png is allowed extension') ;
            }

            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $image->storeAs('public/categories', $filenameToStore);
            $imageName = $filenameToStore;
        }


            $cat = [
                'name' => $request->name,
                'image' => $imageName,
            ];

            $cat = Category::create($cat);

        Flash::success($cat->name.' added successfully');
        return redirect()->route('product_cat.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category::find($id);
//        dd($category);
        return view('categories.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   $cat = Category::find($id);

        if( $request->hasFile('image') ) {


            $image = $request->file('image');

            $valid_extensions = ['svg','png'];

            if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .svg, and .png is allowed extension') ;
            }
            Storage::delete('public/categories/'. $cat->image);

            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $image->storeAs('public/categories', $filenameToStore);
            $imageName = $filenameToStore;

            $cat->name = $request->name;
            $cat->image= $imageName;
            $cat->save();
        }
        else{

            $cat->update($request->all());

        }

        Flash::success($cat->name.' updated successfully');
        return redirect()->route('product_cat.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Category::findOrFail($id);
//        dd($cat);
        Storage::delete('public/categories/' . $cat->image);
        $cat->delete();
        Flash::success('Deleted Successfully');
        return redirect(route('product_cat.index'));
    }
}