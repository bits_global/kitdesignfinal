<?php

namespace App\Http\Controllers;

use App\Design;
use App\Models\Color;
use App\Models\Clothes;
use App\Embellishment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ConfiguratorController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_REQUEST['catID']))
            $catID = $_REQUEST['catID'];
        else
            $catID = '';
		    $colors = Color::all();

        if($catID != '')
            $products = Design::where('prod_id',$catID)->get();
		else
            $products = Design::all();

        $embs = Embellishment::all();
		return view('configurator',compact('colors','products','embs'));
    }


    public static function getCloth($designID)
    {
        return Clothes::where('id',$designID)->get();
    }

    private static function getFiles($path)
    {
        if (is_dir($path)) {
            $files = array_diff(scandir($path), array('.', '..'));
        } else {
            $files = [];
        }
        return $files;
    }
}
