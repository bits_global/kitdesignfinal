<?php

namespace App\Http\Controllers;

use App\Clothes;
use App\Design;
use App\Models\DesignLayers;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;
use UsingRefs\Product;
use DB;

class DesignLayersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designs_layers = DesignLayers::all();
        $designs = Design::all();
        // echo 'Hey Sufyan';exit;
        return view('designs_layers.index',compact('designs_layers','designs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $designs = Design::all();
        return view('designs_layers.create',compact('designs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $designs_layer = [
            'design_id' => $request->design_id,
            'layer_id' => $request->layer_id,
            'layer_title' => $request->layer_title,            
        ];        
        $designs_layer = DesignLayers::create($designs_layer);

        Flash::success($designs_layer->layer_title.' added successfully');
        return redirect()->route('designs_layers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $design = Design::findOrFail($id);
        $designLayers = DesignLayers::where('design_id',$id)->get();
        return view('designs.show',compact('design','designLayers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $designs_layer = DesignLayers::findOrFail($id);
        $designs = Design::all();
        return view('designs_layers.edit',compact('designs','designs_layer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $designs_layer = DesignLayers::whereId($id)->update([
            'design_id' => $request->design_id,
            'layer_id' => $request->layer_id,
            'layer_title' => $request->layer_title,            
        ]);        
        Flash::success('Design Layer updated successfully');
        return redirect()->route('designs_layers.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $designs_layers = DesignLayers::findOrFail($id);
        $designs_layers->delete();
        Flash::success('Design Layers deleted successfully');
        return redirect(route('designs_layers.index'));
    }

    public static function addLayers(Request $request) {
        $input = $request->all();
        $id = $input['id'];
        $layers_no = $input['layer_no'];
        DB::delete('delete from design_layers where design_id = ?',[$id]);
        //$layer = DesignLayers::delete('design_id',$id);
        for($i=0;$i<$layers_no;$i++) {
            $layers = [
                'design_id' => $id,
                'layer_id' => $input['l-'.$i],
                'layer_title' => $input['t-'.$i],
            ];
            Designlayers::insert($layers);
        }
        Flash::success('Layers added successfully');
        return redirect(route('designs.index'));
    }
}
