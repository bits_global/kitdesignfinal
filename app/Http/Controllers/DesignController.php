<?php

namespace App\Http\Controllers;

use App\Clothes;
use App\Design;
use App\Models\DesignLayers;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;
use UsingRefs\Product;
use DB;

class DesignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designs = Design::all();
        return view('designs.index',compact('designs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$products = Clothes::get()->pluck('name', 'id')->prepend('Please select', '');
        $products = Clothes::where('deleted_at',null)->get();
        return view('designs.create',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $request->validate([

            'name' => 'required|string',
            'image' => 'required|image',
            'prod_id' => 'required|number',
            'price' => 'required|float',
            'description' => 'required|string',
        ]);
        */
        $prod_id = $request->input('prod_id');
        $price = $request->input('price');
        $description = $request->input('description');
        $image = $request->file('image');
        $valid_extensions = ['svg'];

        if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
            return redirect()->back()->withInput($request->input())->with('error', 'Only .svg is allowed extension') ;
        }
		$location = public_path() . '/tmp/';

		if (!file_exists($location)) {
			mkdir($location, 0777, true);
		}

		$files = glob($location . '*');
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}
		//$name = $request->myfile->getClientOriginalName();
		$name = time() . '.svg';
		
		//$request->myfile->move($location, $name);
		$image->move($location, $name);
		session(['filename' => $location . $name]);
		// rename($location . $name, public_path().'/images/'. $name);
		$file = basename($location . $name);
		$path = public_path() . '/tmp/' . $name;
		$svg_file = file_get_contents($path);
		$find_string   = '<svg';
		$position = strpos($svg_file, $find_string);
		$svg_file_new = substr($svg_file, $position);

        $design = [
            'name' => $request->name,
            'image' => $svg_file_new,//$imageName,
            'prod_id' => $prod_id,
            'price' => $price,
            'description' => $description,
        ];
        
        $design = Design::create($design);

        Flash::success($design->name.' added successfully');
        return redirect()->route('designs.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $design = Design::findOrFail($id);
        $designLayers = DesignLayers::where('design_id',$id)->get();
        return view('designs.show',compact('design','designLayers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $design = Design::findOrFail($id);
        $products = Clothes::all();
        return view('designs.edit',compact('design','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $design = Design::findOrFail($id);

        /*$request->validate([

            'name' => 'required|string',
            'image' => 'required|image'
        ]);*/

        $filenameToStore = '';
        $image = $request->file('image');
        if($image){
            $valid_extensions = ['svg','png'];
            if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .svg, and .png is allowed extension') ;
            }

            Storage::delete('public/designs/'. $design->image);

            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $image->storeAs('public/designs', $filenameToStore);
            $imageName = $filenameToStore;
        }

        if($filenameToStore){
            $design->image = $filenameToStore ? $filenameToStore : '';
        }
        $design->prod_id = $request->prod_id;
        $design->name = $request->name;
        $design->price = $request->price;
        $design->description = $request->description;
        $design->save();

        Flash::success($design->name.' updated successfully');
        return redirect()->route('designs.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $design = Design::findOrFail($id);
        Storage::delete('public/designs/'.$design->image);
        $design->delete();
        Flash::success($design->name.' deleted successfully');
        return redirect(route('designs.index'));
    }

    public static function addLayers(Request $request) {
        $input = $request->all();
        $id = $input['id'];
        $layers_no = $input['layer_no'];
        DB::delete('delete from design_layers where design_id = ?',[$id]);
        //$layer = DesignLayers::delete('design_id',$id);
        for($i=0;$i<$layers_no;$i++) {
            $layers = [
                'design_id' => $id,
                'layer_id' => $input['l-'.$i],
                'layer_title' => $input['t-'.$i],
            ];
            Designlayers::insert($layers);
        }
        Flash::success('Layers added successfully');
        return redirect(route('designs.index'));
    }
}
