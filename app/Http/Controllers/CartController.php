<?php

namespace App\Http\Controllers;


use App\Design;
use App\Mail\ContactFormMail;
use App\Mail\UserDetails;
use App\Models\Clothes;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use PDF;
//use Session;


class CartController extends Controller
{
    public function addToCart(Request $request)
    {
        session()->flush('userInfo');
        $id          = uniqid();
        $productName = $request->productName;
        $qty         = $request->qty;

        session()->put('userInfo', [
            'base'              => $request->base,
            'overlay'           => $request->overlay,
            'svgData'           => $request->svgData,
            'svgDataStream'     => $request->svgDataStream,
            'baseColor'         => $request->baseColor,
            'layer1Color'       => $request->layer1Color,
            'layer2Color'       => $request->layer2Color,
            'accentColor'       => $request->accentColor,
            'frontTRFile'       => $request->frontTRFile,
            'frontTLFile'       => $request->frontTLFile,
            'frontBRFile'       => $request->frontBRFile,
            'frontBLFile'       => $request->frontBLFile,
            'backCenterFile'    => $request->backCenterFile,
            'frontTRTextFile'   => $request->frontTRTextFile,
            'frontTLTextFile'   => $request->frontTLTextFile,
            'frontBRTextFile'   => $request->frontBRTextFile,
            'frontBLTextFile'   => $request->frontBLTextFile,
            'backCenterTextFile'=> $request->backCenterTextFile,
            'embs'              => $request->embs,
            'productName'       => $request->productName,
            'name'              => $request->name,
            'last_name'         =>$request->last_name,
            'email'             =>$request->email,
            'phone'             =>$request->phone,
            'organization'      =>$request->organization,
            'qty'               =>$request->qty,
            'country'           =>$request->country,
            'city'              =>$request->city,
            'state'             =>$request->state,
            'postalcode'        =>$request->postalcode,
            'comments'          =>$request->comments,
            'PDF_Link' => url('/cartorder') .'/'. time() . '.pdf',
        ]);

        Cart::add($id, $productName, $qty, 12.00);
        return back()->with('Added to Cart');
    }


    public function SendMail(Request $request)
    {
        $data = Session::get('userInfo');

//        $input = $request->all();
//        $product = Design::where('id', $input['svgData'])->get();
//        $cloth = Clothes::where('id', $product[0]->prod_id)->get();
//
//        if($input['svgDataStream']!='' && $product[0]->image!= $input['svgDataStream'])
//        {
//            $product[0]->image_enc = base64_encode( $input['svgDataStream'] );
//        }
//        else
//        {
//            $product[0]->image_enc = base64_encode( $product[0]->image );
//        }
//
//        $time = time();
//        $pdf = PDF::loadView('pdfView', compact('input', 'product','cloth'))->setPaper('a4', 'landscape')->setWarnings(false);
//        $pdf->save(public_path() . '/cartorder/' . $time . '.pdf');
//        $product[0]->stop=true;
//        $PDF = $pdf->download('invoice.pdf');
//
//        if ($PDF)
//        {
//            return $PDF;
//        }
//        $pdf = PDF::loadView('pdfView', compact('input', 'product','cloth'))->setPaper('a4', 'landscape')->setWarnings(false);

        try {
            Mail::to('admin@gmail.com')->send(new ContactFormMail($data));
            Mail::to($data['email'])->send(new UserDetails($data));
            Session::flush();
            session()->flash('message', "Added To Cart ");
            return back();

        } catch (Exception $ex) {
            // Debug via $ex->getMessage();
            session()->flash('message', "We've got errors!");
            return back();
        }
        Session::flush();
        session()->flash('message', "Added To Cart ");
        return back();
    }


//    Working PDF!
//    public function DownloadPDF(Request $request)
//    {
//        $data = Session::get('userInfo');
//        $pdf = PDF::loadView('invoice',compact('data'));
//        $PDF = $pdf->download('Order-Details.pdf');
//
//        if ($PDF)
//        {
//            set_time_limit(300);
//            return $PDF;
//        }
//
//    }
//    Working PDF! End


    public function DownloadPDF(Request $request)
    {
        $data = Session::get('userInfo');
        $input = $request->all();
        $product = Design::where('id', $input['svgData'])->get();
        $cloth = Clothes::where('id', $product[0]->prod_id)->get();

        if($input['svgDataStream']!='' && $product[0]->image!= $input['svgDataStream'])
        {
            $product[0]->image_enc = base64_encode( $input['svgDataStream'] );
        }
        else
        {
            $product[0]->image_enc = base64_encode( $product[0]->image );
        }

        $pdf = PDF::loadView('pdfView', compact('input', 'product','cloth','data'))->setPaper('a4', 'landscape')->setWarnings(false);
        $time = time();
        $pdf->save(public_path() . '/cartorder/' . $time . '.pdf');
        $product[0]->stop=true;

        $PDF = $pdf->download('invoice.pdf');

        if ($PDF)
        {
            return $PDF;
        }

//        $pdf = PDF::loadView('pdfView', compact('input', 'product','cloth','data'))->setPaper('a4', 'landscape')->setWarnings(false);

        session()->flash('message', "Added To Cart ");
        exit;

    }
}
