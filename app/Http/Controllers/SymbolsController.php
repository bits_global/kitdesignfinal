<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSymbolsRequest;
use App\Http\Requests\UpdateSymbolsRequest;
use App\Repositories\SymbolsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SymbolsController extends AppBaseController
{
    /** @var  SymbolsRepository */
    private $SymbolsRepository;

    public function __construct(SymbolsRepository $printsRepo)
    {
        $this->SymbolsRepository = $printsRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the symbols.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->SymbolsRepository->pushCriteria(new RequestCriteria($request));
        $prints = $this->SymbolsRepository->all();

        return view('symbols.index')
            ->with('prints', $prints);
    }

    /**
     * Show the form for creating a new symbols.
     *
     * @return Response
     */
    public function create()
    {
        return view('symbols.create');
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreateSymbolsRequest $request
     *
     * @return Response
     */
    public function store(CreateSymbolsRequest $request)
    {
        $input = $request->all();
        $prints = $this->SymbolsRepository->create($input);

        $location = public_path() . '/tmp/' . $input["symbol"];
        rename($location, public_path() . '/img/symbols/' . $input["symbol"]);

        Flash::success('Symbols saved successfully.');

        return redirect(route('symbols.index'));
    }

    /**
     * Display the specified symbols.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prints = $this->SymbolsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('symbols.index'));
        }

        return view('symbols.show')->with('prints', $prints);
    }

    /**
     * Show the form for editing the specified symbols.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $prints = $this->SymbolsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('symbols.index'));
        }

        return view('symbols.edit')->with('prints', $prints);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdateSymbolsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSymbolsRequest $request)
    {
        $prints = $this->SymbolsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('symbols.index'));
        }

        $prints = $this->SymbolsRepository->update($request->all(), $id);

        Flash::success('Symbols updated successfully.');

        return redirect(route('symbols.index'));
    }

    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prints = $this->SymbolsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('symbols.index'));
        }

        $this->SymbolsRepository->delete($id);

        Flash::success('Symbols deleted successfully.');

        return redirect(route('symbols.index'));
    }

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.svg';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }
    }
}
