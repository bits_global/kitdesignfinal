<?php

namespace App\Http\Controllers;

use App\Embellishment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class EmbellishmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $embs = Embellishment::all();
        return view('embellishments.index',compact('embs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('embellishments.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ]);
        $imageName1 = '';
        $imageName2 = '';
        /*dd($request->hasFile('thumb')." - ".$request->hasFile('file'));
        if ($request->hasFile() == true) {
            // Print the real file names and their sizes
            foreach ($request->getUploadedFiles() as $file) {
                echo $file->getName(), " ", $file->getSize(), "\n";
            }
        }else{
            echo "No upload file"; //<========== CAME HERE!!
        }
        exit;
        */
        if( $request->hasFile('thumb') ) {
            $image = $request->file('thumb');
            $valid_extensions = ['png','jpg'];

            if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .svg, .jpg and .png is allowed extension') ;
            }
            /*****New Code*****/
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.png';
            $request->thumb->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            //$filenameWithExt = $image->getClientOriginalName();
            $location = public_path() . '/tmp/' . $file;
            rename($location, public_path() . '/img/embellishments/thumbs/' . $file);
            $imageName2 = $file;
            /*****Old Code*****/
            /*
            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $image->storeAs('public/embellishments/thumbs', $filenameToStore);
            $imageName2 = $filenameToStore;
            */
        }

        //echo "File: ".$request->hasFile('file');

        if( $request->hasFile('file') ) {
            echo "file";
            $image = $request->file('file');
            $valid_extensions = ['svg'];

            if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .svg is allowed extension') ;
            }
            /*****New Code*****/
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.svg';
            $request->file->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);

            //$filenameWithExt = $image->getClientOriginalName();
            $location = public_path() . '/tmp/' . $file;
            rename($location, public_path() . '/img/embellishments/' . $file);
            $imageName1 = $file;
            /*****Old Code*****/
            /*
            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $image->storeAs('public/embellishments', $filenameToStore);
            $imageName1 = $filenameToStore;
            */
        }

        $embs = [
            'name' => $request->name,
            'file' => $imageName1,
            'thumb' => $imageName2,
        ];
        /*echo "<pre>";
        print_r($embs);
        echo "<pre>";
        exit;*/
        $emb = Embellishment::create($embs);

        Flash::success($emb->name.' added successfully');
        return redirect()->route('embs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('embellishments.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emb = Embellishment::find($id);

        return view('embellishments.edit',compact('emb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emb = Embellishment::findOrFail($id);
        if( $request->hasFile('file') ) {


            $image = $request->file('file');

            $valid_extensions = ['svg','png'];

            if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .svg, and .png is allowed extension') ;
            }
            Storage::delete('public/embellishments/'. $emb->file);

            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $image->storeAs('public/embellishments', $filenameToStore);
            $imageName1 = $filenameToStore;

            $emb->name = $request->name;
            $emb->file= $imageName1;
            $emb->save();

        }elseif( $request->hasFile('thumb') ) {


            $image = $request->file('thumb');

            $valid_extensions = ['svg','png'];

            if ( ! in_array( strtolower( $image->getClientOriginalExtension()), $valid_extensions) ) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .svg, and .png is allowed extension') ;
            }

            Storage::delete('public/embellishments/thumbs/'. $emb->file);

            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $image->storeAs('public/embellishments', $filenameToStore);
            $imageName2 = $filenameToStore;

            $emb->name = $request->name;
            $emb->thumb= $imageName2;
            $emb->save();
        }else{
            $emb->update($request->all());
        }

        Flash::success($emb->name.' updated successfully');
        return redirect()->route('embs.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emb = Embellishment::findOrFail($id);
//        dd($cat);
        Storage::delete('public/embellishments/' . $emb->file);
        Storage::delete('public/embellishments/thumbs/' . $emb->thumb);
        $emb->delete();
        Flash::success('Deleted Successfully');
        return redirect(route('embs.index'));
    }
}
