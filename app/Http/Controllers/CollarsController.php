<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCollarsRequest;
use App\Http\Requests\UpdateCollarsRequest;
use App\Repositories\CollarsRepository;
use App\Category;
use App\Clothes;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CollarsController extends AppBaseController
{
    /** @var  CollarsRepository */
    private $CollarsRepository;

    public function __construct(CollarsRepository $printsRepo)
    {
        $this->CollarsRepository = $printsRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the collars.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->CollarsRepository->pushCriteria(new RequestCriteria($request));
        $prints = $this->CollarsRepository->all();

        return view('collars.index')
            ->with('prints', $prints);
    }

    /**
     * Show the form for creating a new collars.
     *
     * @return Response
     */
    public function create()
    {
		$catArr = Clothes::whereNull('deleted_at')->get();
		$tempCat = array();
		foreach($catArr as $cat)
		{
			$tempCat[$cat->id] = $cat->name;
		}
		//return response()->json(['cat' => $cat]);
        return view('collars.create')->with('cat', $tempCat);
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreateCollarsRequest $request
     *
     * @return Response
     */
    public function store(CreateCollarsRequest $request)
    {
        $input = $request->all();
        $prints = $this->CollarsRepository->create($input);

        //$location = public_path() . '/tmp/' . $input["collar"];
        //rename($location, public_path() . '/img/collars/' . $input["collar"]);

        Flash::success('Collars saved successfully.');

        return redirect(route('collars.index'));
    }

    /**
     * Display the specified collars.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prints = $this->CollarsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Collars not found');

            return redirect(route('collars.index'));
        }

        return view('collars.show')->with('prints', $prints);
    }

    /**
     * Show the form for editing the specified collars.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $prints = $this->CollarsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Collars not found');

            return redirect(route('collars.index'));
        }

        return view('collars.edit')->with('prints', $prints);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdateCollarsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCollarsRequest $request)
    {
        $prints = $this->CollarsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Collars not found');

            return redirect(route('collars.index'));
        }

        $prints = $this->CollarsRepository->update($request->all(), $id);

        Flash::success('Collars updated successfully.');

        return redirect(route('collars.index'));
    }

    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prints = $this->CollarsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Collars not found');

            return redirect(route('collars.index'));
        }

        $this->CollarsRepository->delete($id);

        Flash::success('Collars deleted successfully.');

        return redirect(route('collars.index'));
    }

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            //$name = $request->myfile->getClientOriginalName();
			$name = time() . '.svg';
            
			$request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            $path = public_path() . '/tmp/' . $name;
			$svg_file = file_get_contents($path);
			$find_string   = '<svg';
			$position = strpos($svg_file, $find_string);
			$svg_file_new = substr($svg_file, $position);
			return "" . $svg_file_new;
			//return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }
    }
	
	public static function getCategoryName($catID)
	{
		$catArr = Clothes::where('id',$catID)->where('deleted_at',null)->get();
		return $catID;//$catArr[0]->name;
	}
}
