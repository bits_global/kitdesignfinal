<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAlphabetsRequest;
use App\Http\Requests\UpdateAlphabetsRequest;
use App\Repositories\AlphabetsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AlphabetsController extends AppBaseController
{
    /** @var  AlphabetsRepository */
    private $AlphabetsRepository;

    public function __construct(AlphabetsRepository $printsRepo)
    {
        $this->AlphabetsRepository = $printsRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the alphabets.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->AlphabetsRepository->pushCriteria(new RequestCriteria($request));
        $prints = $this->AlphabetsRepository->all();

        return view('alphabets.index')
            ->with('prints', $prints);
    }

    /**
     * Show the form for creating a new alphabets.
     *
     * @return Response
     */
    public function create()
    {
        return view('alphabets.create');
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreateAlphabetsRequest $request
     *
     * @return Response
     */
    public function store(CreateAlphabetsRequest $request)
    {
        $input = $request->all();
        //dd($input);
        $prints = $this->AlphabetsRepository->create($input);

        $location = public_path() . '/tmp/' . $input["alphabet"];
        rename($location, public_path() . '/img/alphabets/' . $input["alphabet"]);

        Flash::success('Symbols saved successfully.');

        return redirect(route('alphabets.index'));
    }

    /**
     * Display the specified alphabets.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prints = $this->AlphabetsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('alphabets.index'));
        }

        return view('alphabets.show')->with('prints', $prints);
    }

    /**
     * Show the form for editing the specified alphabets.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $prints = $this->AlphabetsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('alphabets.index'));
        }

        return view('alphabets.edit')->with('prints', $prints);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdateAlphabetsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlphabetsRequest $request)
    {
        $prints = $this->AlphabetsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('alphabets.index'));
        }

        $prints = $this->AlphabetsRepository->update($request->all(), $id);

        Flash::success('Symbols updated successfully.');

        return redirect(route('alphabets.index'));
    }

    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prints = $this->AlphabetsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Symbols not found');

            return redirect(route('alphabets.index'));
        }

        $this->AlphabetsRepository->delete($id);

        Flash::success('Symbols deleted successfully.');

        return redirect(route('alphabets.index'));
    }

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.svg';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }
    }
}
