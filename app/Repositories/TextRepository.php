<?php

namespace App\Repositories;

use App\Models\Text;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PrintsRepository
 * @package App\Repositories
 * @version May 2, 2018, 6:21 am UTC
 *
 * @method Prints findWithoutFail($id, $columns = ['*'])
 * @method Prints find($id, $columns = ['*'])
 * @method Prints first($columns = ['*'])
*/
class TextRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Text::class;
    }
}
