<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Clothes
 * @package App\Models
 * @version May 2, 2018, 5:19 am UTC
 *
 * @property string name
 * @property string image
 */
class Clothes extends Model
{
    use SoftDeletes;

    public $table = 'clothes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'image',
        'base',
        'overlay',
        'thumb',
		'frontTL',
		'frontTLText',
		'frontTR',
		'frontTRText',
		'frontBL',
		'frontBLText',
		'frontBR',
		'frontBRText',
		'backCenter',
		'backCenterText'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
	 	'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'base'  => 'string',
        'overlay' => 'string',
        'thumb' => 'string',
		'frontTL' => 'string',
		'frontTLText' => 'string',
		'frontTR' => 'string',
		'frontTRText' => 'string',
		'frontBL' => 'string',
		'frontBLText' => 'string',
		'frontBR' => 'string',
		'frontBRText' => 'string',
		'backCenter' => 'string',
		'backCenterText'=> 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
