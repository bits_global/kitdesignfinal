<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Prints
 * @package App\Models
 * @version May 2, 2018, 6:21 am UTC
 *
 * @property string name
 * @property string image
 */
class Alphabets extends Model
{
    use SoftDeletes;

    public $table = 'alphabets';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'alphabet',
        'flag',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'alphabet' => 'string',
        'flag' => 'int'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}