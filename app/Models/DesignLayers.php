<?php

namespace App\Models;

use Eloquent as Model;

class DesignLayers extends Model
{
    protected $fillable = [
        'design_id',
        'layer_id',
        'layer_title'
    ];
}
