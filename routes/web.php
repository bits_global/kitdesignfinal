<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect(route('home'));
//    return view('empty');
});


Route::get('storage/{filename}', function ($filename)
{
    return Image::make(storage_path('public/' . $filename))->response();
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products', 'HomeController@productsHome')->name('product');
Route::get('/faqs', 'FaqsFrontController@indexHome')->name('faqs');
Route::get('/products', 'HomeController@productsHome')->name('product');
Route::get('/how-to-order', 'HomeController@orderHome')->name('how-to-order');
Route::get('/get-in-touch', 'HomeController@contactHome')->name('get-in-touch');
Route::get('/our-team', 'TeamsFrontController@indexHome')->name('our-team');
Route::get('/design-lab', 'ConfiguratorController@index')->name('configurator');
Route::get('/viewcart', 'step1Controller@cart')->name('cart');
Route::patch('/cart_update/{rowId}', 'step1Controller@cart_update')->name('cart_update');
Route::patch('/cart_decrease/{rowId}', 'step1Controller@cart_decrease')->name('cart_decrease');
Route::patch('/remove/{rowId}', 'step1Controller@remove')->name('remove');
Route::post('/checkout', 'step1Controller@paypal')->name('checkout');
Route::get('/paypal', 'step1Controller@sucess')->name('sucess');

Route::get('/step1-addColor', 'step1Controller@index')->name('step1-addColor');
Route::post('/step1-addColor/uploade', 'step1Controller@uploade')->name('step1-addColor.uploade');
Route::post('/step1-addColor/download-pdf','step1Controller@downloadPDF')->name('step1-addColor.download-pdf');

Route::get('/basic_email', 'step1Controller@basic_email')->name('basic_email');

Route::post('add-to-cart','CartController@addToCart')->name('addToCart');

Route::post('send.mail','CartController@SendMail')->name('send.mail');
Route::post('downloadPDF','CartController@DownloadPDF')->name('download.PDF');

Route::get('/invoice', function () {
    return view('invoice');
//    $pdf = PDF::loadView('invoice');
//    return $pdf->download('Customer-Data.pdf');
});

/*Route::group(['prefix'=>'configurator'],function (){
    Route::resource('step1addColor', 'step1Controller');
});*/

Route::get('/admin', function () {
    return redirect(route('users.index'));
});
Route::group(['prefix'=>'admin'],function (){
    Route::resource('colors', 'ColorController');
    Route::resource('clothes', 'ClothesController');
    Route::post('/clothes/uploade', 'ClothesController@uploade')->name('clothes.uploade');
    Route::resource('users', 'UsersController');
    Route::resource('prints', 'PrintsController');
    Route::resource('product_cat','CategoryController');
    Route::resource('designs','DesignController');
    Route::resource('designs_layers','DesignLayersController');
	Route::post('/designs/addLayers', 'DesignController@addLayers')->name('designs.addLayers');
    Route::resource('clip_arts','ClipArtController');
    Route::resource('fonts','FontController');
    Route::resource('embs','EmbellishmentController');
    Route::resource('sizes','SizeController');
    Route::post('/uploade','PrintsController@uploade')->name('uploade');
    Route::resource('homepage_banners', 'HomepageBannersController');
	Route::resource('text', 'TextController');
	Route::resource('teams', 'TeamsController');
	Route::resource('faqs', 'FaqsController');
	Route::resource('editable_logos', 'EditableLogosController');
	Route::post('/editable_logos/uploade', 'EditableLogosController@uploade')->name('editable_logos.uploade');
	Route::resource('symbols', 'SymbolsController');
	Route::post('/symbols/uploade', 'SymbolsController@uploade')->name('symbols.uploade');
	Route::resource('alphabets', 'AlphabetsController');
	Route::post('/alphabets/uploade', 'AlphabetsController@uploade')->name('alphabets.uploade');
	Route::resource('collars', 'CollarsController');
	Route::post('/collars/uploade', 'CollarsController@uploade')->name('collars.uploade');
	Route::resource('coming_soon', 'ComingSoonController');
});