/*contact page map overlay */
	var map ;
	var brooklyn = new google.maps.LatLng(40.6743890, -73.9455);
	var MY_MAPTYPE_ID = 'custom_style';
	function initialize() {
	    var featureOpts = [ {
	    featureType: "all",
	    stylers: [
	      { saturation: -100 }
	    ]
	  },{
	    featureType: "road.arterial",
	    elementType: "geometry",
	    stylers: [
	      { hue: "#000" },
	      { saturation: 50 }
	    ]
	  },{
	    featureType: "poi.business",
	    elementType: "labels",
	    stylers: [
	      { visibility: "off" }
	    ]
	  }
	];
    var mapOptions = {
        zoom: 12,
        center: brooklyn,
        scrollwheel: false,
        mapTypeId: MY_MAPTYPE_ID
    };

    var customMapType = new google.maps.StyledMapType(featureOpts);
    
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
  }
  google.maps.event.addDomListener(window, 'load', initialize);