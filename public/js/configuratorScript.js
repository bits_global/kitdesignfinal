// JavaScript Document

var defaultBaseColor;
var defaultLayer1Color;
var defaultLayer2Color;
var defaultAccentColor;

function getDefaultColors() {
	/**/
	var svgRect = document.getElementById("Layer_2");
	var svgBody = svgRect.getElementById("layer-1");
	defaultBaseColor = svgBody.getAttribute("fill");
	document.getElementById("baseColor").value = defaultBaseColor;
	var svgLayer1 = svgRect.getElementById("layer-2");
	defaultLayer1Color = svgLayer1.getAttribute("fill");
	document.getElementById("layer1Color").value = defaultLayer1Color;
	var svgLayer2 = svgRect.getElementById("layer-3");
	defaultLayer2Color = svgLayer2.getAttribute("fill");
	document.getElementById("layer2Color").value = defaultLayer2Color;
    var svgAccents = svgRect.getElementById("layer-4");
	defaultAccentColor = svgAccents.getAttribute("fill");
	document.getElementById("accentColor").value = defaultAccentColor;
}

function changeBaseColor(baseColor) {
	var elms = document.querySelectorAll("[id='layer-1']");

	for(var i = 0; i < elms.length; i++) 
	  elms[i].setAttribute("fill",baseColor);
	console.log("Base updated");
}

function changeLayer1Color(layer1Color) {
	var elms = document.querySelectorAll("[id='layer-2']");

	for(var i = 0; i < elms.length; i++) 
	  elms[i].setAttribute("fill",layer1Color);
	console.log("Layer1 updated");
}

function changeLayer2Color(layer2Color) {
	var elms = document.querySelectorAll("[id='layer-3']");

	for(var i = 0; i < elms.length; i++) 
	  elms[i].setAttribute("fill",layer2Color);
	console.log("Layer2 updated");
}

function changeLayer3Color(layer3Color) {
	var elms = document.querySelectorAll("[id='layer-4']");

	for(var i = 0; i < elms.length; i++) 
	  elms[i].setAttribute("fill",layer3Color);
	console.log("Layer3 updated");
}

function changeS1Colors(color, layer) {
	var elms = document.querySelectorAll("[id='"+layer+"']");

	for(var i = 0; i < elms.length; i++) 
	  elms[i].setAttribute("fill",color);
}

function changeColor(color, location, parentDiv) {
	var parent = document.getElementById(parentDiv);
	var svgRect = parent.querySelector('svg');
	var svgAccents = svgRect.getElementById(location);
	svgAccents.setAttribute("fill",color);
	$('#'+parentDiv+'_fonts_abc').css({'color': color});
}

/*function colorReset() {
	var svgRect = document.getElementById("Layer_1");
	var svgBody = svgRect.getElementById("Body");
	svgBody.setAttribute("fill",defaultBaseColor);
	var svgLayer1 = svgRect.getElementById("Layer-01");
	svgLayer1.setAttribute("fill",defaultLayer1Color);
	var svgLayer2 = svgRect.getElementById("Layer-02");
	svgLayer2.setAttribute("fill",defaultLayer2Color);
    var svgAccents = svgRect.getElementById("Accents");
	svgAccents.setAttribute("fill",defaultAccentColor);
}
*/
function addFrontTR(file,location) {
	var alpha1 =  $('#alpha1').attr('fill');
	var alpha2 =  $('#alpha2').attr('fill');
	var alpha3 =  $('#alpha3').attr('fill');
	
	var position = document.getElementById(location);
	var fileData = readFile(file);
	position.innerHTML = fileData;
	document.getElementById('frontTRFile').value = file;
	$('#alpha1').attr('fill', alpha1);
	$('#alpha2').attr('fill', alpha2);
	$('#alpha3').attr('fill', alpha3);
	// document.getElementById('myModal1-1-1').style.display='';
	// document.getElementById('preview-1-1').innerHTML = fileData;
}

function addFrontTL(file,location) {
	var alpha1 =  $('#alpha1').attr('fill');
	var alpha2 =  $('#alpha2').attr('fill');
	var alpha3 =  $('#alpha3').attr('fill');

	var position = document.getElementById(location);
	var fileData = readFile(file);
	position.innerHTML = fileData;
	document.getElementById('frontTLFile').value = file;
	$('#alpha1').attr('fill', alpha1);
	$('#alpha2').attr('fill', alpha2);
	$('#alpha3').attr('fill', alpha3);
	// document.getElementById('myModal1-2-2').style.display='';
	// document.getElementById('preview-2-2').innerHTML = fileData;
}

function addFrontBR(file,location) {
	var alpha1 =  $('#alpha1').attr('fill');
	var alpha2 =  $('#alpha2').attr('fill');
	var alpha3 =  $('#alpha3').attr('fill');

	var position = document.getElementById(location);
	var fileData = readFile(file);
	position.innerHTML = fileData;
	document.getElementById('frontBRFile').value = file;
	$('#alpha1').attr('fill', alpha1);
	$('#alpha2').attr('fill', alpha2);
	$('#alpha3').attr('fill', alpha3);
	// document.getElementById('myModal1-3-3').style.display='';
	// document.getElementById('preview-3-3').innerHTML = fileData;
}

function addFrontBL(file,location) {
	var alpha1 =  $('#alpha1').attr('fill');
	var alpha2 =  $('#alpha2').attr('fill');
	var alpha3 =  $('#alpha3').attr('fill');

	var position = document.getElementById(location);
	var fileData = readFile(file);
	position.innerHTML = fileData;
	document.getElementById('frontBLFile').value = file;
	$('#alpha1').attr('fill', alpha1);
	$('#alpha2').attr('fill', alpha2);
	$('#alpha3').attr('fill', alpha3);
	// document.getElementById('myModal1-4-4').style.display='';
	// document.getElementById('preview-4-4').innerHTML = fileData;
}

function addBackCenter(file,location) {
	var alpha1 =  $('#alpha1').attr('fill');
	var alpha2 =  $('#alpha2').attr('fill');
	var alpha3 =  $('#alpha3').attr('fill');

	var position = document.getElementById(location);
	var fileData = readFile(file);
	position.innerHTML = fileData;
	document.getElementById('backCenterFile').value = file;
	$('#alpha1').attr('fill', alpha1);
	$('#alpha2').attr('fill', alpha2);
	$('#alpha3').attr('fill', alpha3);
	// document.getElementById('myModal1-5-5').style.display='';
	// document.getElementById('preview-5-5').innerHTML = fileData;

}

function addOutline()
{
	var w = document.getElementById('Layer_2');
	var x = w.getElementsByTagName('path');
	var i;
	for (i = 0; i < x.length; i++) {
	  x[i].style = 'stroke: #ffffff; stroke-width: 2px; stroke-dasharray: 2,2; stroke-linejoin: round;';
	}
}

function removeOutline()
{
	var w = document.getElementById('Layer_2');
	var x = w.getElementsByTagName('path');
	var i;
	for (i = 0; i < x.length; i++) {
	  x[i].style = 'stroke: #ffffff; stroke-width: 0px; stroke-dasharray: 2,2; stroke-linejoin: round;';
	}
}

/*function addText(input, location, css)
{
	var position = document.getElementById(location+'Text');
	var e = document.getElementById("fonts_abc");
	var fonts = e.options[e.selectedIndex].value;
	console.log(fonts);
	position.innerHTML = "<strong style='font-family:'"+fonts+";'>"+input+"</strong>";
}*/

function readFile(file)
{
	var ext = getExt(file);
	if(ext == "svg") {
		var rawFile = new XMLHttpRequest();
	    rawFile.open("GET", file, false);
	    var allText;
	    rawFile.onreadystatechange = function ()
	    {
	        if(rawFile.readyState === 4)
	        {
	            if(rawFile.status === 200 || rawFile.status == 0)
	            {
	                allText = rawFile.responseText;
	            }
	        }
	    }
	    rawFile.send(null);
	    return allText;
	}
	else {
		return "<img class='img-fluid col-10' style='padding-top: 20px' src='"+file+"'>";
	}   
}


function getExt(filename)
{
    var ext = filename.split('.').pop();
    if(ext == filename) return "";
    return ext;
}

function applyEmb(file,id) {
	document.getElementById("loading_box").style.display="block";
	var embs = document.querySelectorAll("[id='emb']");
	//var a = document.querySelectorAll("[id='productLink']");
	var a = document.querySelectorAll('.productLink');
			
	if(file!='none') {

		console.log("file");

		for(var i = 0; i < embs.length; i++) { 
			embs[i].innerHTML = '<img src="'+file+'" style="height: 140px; width: 286px;">';
		}

		for(var i = 0; i < a.length; i++) {
			href = a[i].getAttribute("href")+"&emb="+id;
			//console.log(href);
			a[i].setAttribute("href",href);
		}

	}
	else {
		console.log("none");
		for(var i = 0; i < embs.length; i++) 
			embs[i].innerHTML = '';
	}

			
		
		setTimeout(function(){ 
		/*	stupid requirement by client and my contractor. without realiing that old dev did a number on them.*/
        document.getElementById("loading_box").style.display="none";	
    	
		}, 1000);  
	
		
}

function applyEmbStep1(file) {

	document.getElementById('embLayer').innerHTML = readFile(file);
}

function changeEmbColor(color) {
	document.getElementsByClassName("st3")[0].style.fill=color;
}