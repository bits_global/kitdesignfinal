<!doctyspane html>
<html lang="en">
<head>
	<style>
		a:link, a:visited {
		  background-color: rgb(66, 162, 244);
		  color: white;
		  padding: 14px 25px;
		  text-align: center;
		  text-decoration: none;
		  display: inline-block;
		}

		a:hover, a:active {
		  background-color: rgb(66, 162, 244);
		}
	</style>
</head>

<body>
	<h5>Hi, {{ $data['name'] }}</h5>
	<span>Your design details are below:</span><br><br>
	<span><strong>Email : </strong>{{ $data['email'] }}</span><br>
	<span><strong>Phone# : </strong>{{ $data['phone'] }}</span><br>
	<span><strong>Organization : </strong>{{ $data['organization'] }}</span><br>
	<span><strong>Quantity : </strong>{{ $data['qty'] }}</span><br>
	<span><strong>Country : </strong>{{ $data['country'] }}</span><br>
	<span><strong>City : </strong>{{ $data['city'] }}</span><br>
	<span><strong>State : </strong>{{ $data['state'] }}</span><br>
	<span><strong>Postal Code : </strong>{{ $data['postalcode'] }}</span><br>
	<span><strong>Comments : </strong>{{ $data['comments'] }}</span><br>
	<span><strong>PDF Download : </strong> <a target="_blank" href="{{ $data['PDF_Link'] }}">Download PDF</a></span>
</body>
</html>