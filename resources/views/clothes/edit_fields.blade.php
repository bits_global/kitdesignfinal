<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', $clothes['name'], ['class' => 'form-control']) !!}
</div>

<!-- new fields added by jawad to fix the text overlap problem on t-shirt and pajamas. -->

<div class="form-group col-sm-6">
    {!! Form::label('frontTL', 'Front Layer Left:') !!}
    {!! Form::text('frontTL', null, ['class' => 'form-control','placeholder'=>'top:125px; left:110px;']) !!}
</div>



<div class="form-group col-sm-6">
    {!! Form::label('frontTLText', 'Front Text Layer Left:') !!}
    {!! Form::text('frontTLText', null, ['class' => 'form-control','placeholder'=>'top:205px; left:110px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('frontTR', 'Front Layer Right:') !!}
    {!! Form::text('frontTR', null, ['class' => 'form-control','placeholder'=>'top:125px; left:245px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('frontTRText', 'Front Text Layer Right:') !!}
    {!! Form::text('frontTRText', null, ['class' => 'form-control','placeholder'=>'top:185px; left:245px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('frontBL', 'Front Bottom Left:') !!}
    {!! Form::text('frontBL', null, ['class' => 'form-control','placeholder'=>'top:340px; left:100px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('frontBLText', 'Front Bottom Left Text:') !!}
    {!! Form::text('frontBLText', null, ['class' => 'form-control','placeholder'=>'top:420px; left:100px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('frontBR', 'Front Bottom Right:') !!}
    {!! Form::text('frontBR', null, ['class' => 'form-control','placeholder'=>'top:340px; left:257px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('frontBRText', 'Front Bottom Right Text:') !!}
    {!! Form::text('frontBRText', null, ['class' => 'form-control','placeholder'=>'top:420px; left:257px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('backCenter', 'Back Center:') !!}
    {!! Form::text('backCenter', null, ['class' => 'form-control','placeholder'=>'top:90px; left:567px;']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('backCenterText', 'Back Center Text:') !!}
    {!! Form::text('backCenterText', null, ['class' => 'form-control','placeholder'=>'top:230px; left:575px;']) !!}
</div>


<!-- new fields ends here -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::hidden('id', $clothes['id'], ['class' => 'form-control']) !!}
    {!! Form::hidden('base', null, ['class' => 'form-control image']) !!}
    {!! Form::hidden('overlay', null, ['class' => 'form-control image']) !!}
    {!! Form::hidden('thumb', null, ['class' => 'form-control image']) !!}
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('clothes.index') !!}" class="btn btn-default">Cancel</a>
</div>
