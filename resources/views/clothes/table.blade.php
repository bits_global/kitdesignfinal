<style>
    .cloth{
        width: 150px;
        max-width: 150px;
        max-height: 200px;
    }
</style>
<table class="table table-responsive" id="dataTable">
    <thead>
        <tr>
            <th>Name</th>
            <th>Thumb</th>
			<th>Images</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($clothes as $clothes)
        <tr>
            <td>{!! $clothes->name !!}</td>
            <td>
                <img class="img-fluid cloth" src="{{ asset('img/t-shirts/') }}/{!! $clothes->base !!}_thumb.png">
            </td>
            <td>
                <img class="cloth" src="{{ asset('img/t-shirts/') }}/{!! $clothes->base !!}_base.png">
            </td>
            <td>
                {!! Form::open(['route' => ['clothes.destroy', $clothes->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <?php /*?><a href="{!! route('clothes.show', [$clothes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a><?php */?>
                    <a href="{!! route('clothes.edit', [$clothes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>