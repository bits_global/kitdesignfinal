@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Design Create
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                {!! Form::open(['route' => 'designs.store','enctype'=>'multipart/form-data']) !!}

                    <div class="form-group col-sm-6">
                        {!! Form::label('prod_id', 'Product') !!}
                        <select class='form-control' id='prod_id' name="prod_id">
							<option>Select Product</option>
							@foreach($products as $product)
							<option value='{!! $product->id !!}'>{!! $product->name !!}</option>
							@endforeach
						</select>
                    </div>
                    
                    <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- File Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('image', 'Image') !!}
                        {!! Form::file('image', ['class' => 'form-control']) !!}
                    </div>

                    <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('price', 'Price:') !!}
                        {!! Form::text('price', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Name Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('description', 'Description:') !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    </div>


                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('designs.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
