<?php
$homeNav = '';
$productNav = '';
$designLabNav = '';
$teamNav = ' active';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>
<!doctype html>
<html lang="en">
	<head>
		@include('layouts.front_header')
	</head>
	<body>
		@include('layouts.front_nav')
		<!-- Content -->
		<div class="container">
         <!--our team -->
         <div class="row">
            <div class="our-team">
               <div class="col-md-12">
                  <h2 class="text-center">OUR TEAM</h2>
               </div>
            </div>
            <div class="container" style="height:100vh;">
               <div class="row">
			   @foreach($textArr as $teamMember)
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-6">
                           <div class="border-right-our-team">
                              <img src="{{ asset('img/team/') }}/{!! $teamMember->image !!}" alt="" class="img-fluid circle-shape">
                           </div>
                        </div>
                        <div class="col-6">
                           <div class="our-team-sec">
                              <h4>{!! $teamMember->name !!}</h4>
                              <p class="team-discription-text">
                                 {!! $teamMember->description !!}
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
				@endforeach
               </div>
            </div>
         </div>
      </div>
		@include('layouts.front_footer')
	</body>
	@include('layouts.front_footerScript')
</html>