<style>
    .tt{
        width: 100px;
        max-width: 100px;
        max-height: 100px;
    }
</style>
<table class="table table-responsive"  id="dataTable">
    <thead>
        <tr>
			<th>Alphabet</th>
            <th>Name</th>
			<th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($prints as $prints)
        <tr>
            <td>
                <img class="tt" src="{{ asset('img/alphabets/') }}/{!! $prints->alphabet !!}">
            </td>
			<td>
				{!! $prints->name !!}
			</td>
            <td>
                {!! Form::open(['route' => ['alphabets.destroy', $prints->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('alphabets.show', [$prints->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<?php /*?>
					<a href="{!! route('alphabets.edit', [$prints->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
					<?php */?>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>