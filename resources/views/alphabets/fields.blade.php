<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6 image">
    {!! Form::label('alphabet', 'Alphabet:') !!}
    {!! Form::text('alphabet', null, ['class' => 'form-control image']) !!}
</div>
<div class="form-group col-sm-12">
    {!! Form::label('flag', 'Type') !!}
    <select class='form-control' id='flag' name="flag">
		<option>Select Alphabet Type</option>
		<option value='0'>Alphabets</option>
		<option value='1'>Digits</option>
	</select>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('alphabets.index') !!}" class="btn btn-default">Cancel</a>
</div>
