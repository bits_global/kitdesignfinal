@component('mail::message')
# You Received a new Order!

<h2><strong>Customer Data</strong></h2><br>
<strong>Name:</strong> {{ $data['name'] }} {{ $data['last_name'] }} <br>
<strong>Email:</strong> {{ $data['email'] }} <br>
<strong>Organization:</strong> {{ $data['organization'] }} <br>
<strong>Quantity:</strong> {{ $data['qty'] }} <br>
<strong>Country:</strong> {{ $data['country'] }} <br>
<strong>City:</strong> {{ $data['city'] }} <br>
<strong>Postal Code:</strong> {{ $data['postalcode'] }} <br>
<strong>Comments:</strong> {{ $data['comments'] }} <br><br>

<span><strong>PDF Download : </strong> <a target="_blank" href="{{ $data['PDF_Link'] }}">Download PDF</a></span>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
