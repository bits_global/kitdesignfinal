@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Embellishments
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                {!! Form::model($emb, ['route' => ['embs.update', $emb->id],'enctype'=>'multipart/form-data','method' => 'patch']) !!}


                <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name',old('name'), ['class' => 'form-control']) !!}
                    </div>

                    <!-- Image Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('file', 'File') !!}
                        {!! Form::file('file', null, ['class' => 'form-control image']) !!}

                    </div>

                    <div class="form-group col-sm-6">
                        {!! Form::label('thumb', 'Thumb') !!}
                        {!! Form::file('thumb', null, ['class' => 'form-control image']) !!}

                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('embs.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection