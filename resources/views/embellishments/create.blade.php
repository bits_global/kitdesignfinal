@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Embellishments Create
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'embs.store', 'file' => true, 'enctype'=>'multipart/form-data']) !!}

                    <!-- Name Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Thumb Image Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('thumb', 'Thumb') !!}
                            {!! Form::file('thumb', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Embs Image Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('file', 'File') !!}
                            {!! Form::file('file', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('embs.index') !!}" class="btn btn-default">Cancel</a>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
