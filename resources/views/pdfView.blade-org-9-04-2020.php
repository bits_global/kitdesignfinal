<!DOCTYPE html>
<html>
<head>
	<title>KIT DESIGN - PDF</title>

	<style type="text/css">
		.h_w_cls{
			height:60px; 
			width:60px;
		}
		.img-base{
			width:147%;
		}
		@media only screen and (max-width: 768px) {
        .img-base{
			width:115% !important;
		}
        }
		
	</style>
</head>
<body>
	<h2 align="center">KIT DESIGN</h2><hr>
	<?php echo $embs = asset("img/embellishments/") . "/" . $input['embs']; ?>

	@if($input['embs'] != '')
    	<div class="col-12" style="z-index: 2; width: 97%; height: 90% !important; cursor:pointer; position:absolute;"
         id="embLayer"></div>
	@endif 
    <div class="img-fluid" style="z-index: 3; cursor:pointer; position:absolute;">
		<img  src="{!! $input['base'] !!}" class='col-12 img-base'>
	</div>
	<div class="img-fluid" style="z-index: 4; cursor:pointer; position:absolute;">
		<img  src="{!! $input['overlay'] !!}" class='col-12 img-base'>
	</div>
    @if($input['frontTLFile']!='')
		<div style="z-index:2; position:absolute; top:125px; left:110px;" class="h_w_cls" id='frontTL'>
			<img src="{!! $input['frontTLFile'] !!}" class='col-12'>
		</div>		
	@endif 
	@if($input['frontTLTextFile']!='')
	<div class="test" style="z-index:2; position:absolute; top:205px; left:110px; height:12pt; width:100%; color: #ffffff;" id="frontTLText">
		<strong id="frontTL_fonts_abc" style="{!! $input['frontTLTextFile'] !!}">{!! $input['frontTLTextFile'] !!}</strong>
	</div>
	@endif

	@if($input['frontTRFile']!='')
		<div style="z-index:2;position:absolute;top: 185px;left: 215px;" class="h_w_cls" id='frontTR'>
			<img src="{!! $input['frontTRFile'] !!}" class='col-12'>
		</div>
	@endif
	@if($input['frontTRTextFile']!='')
	<div class="test" style="z-index:2; position:absolute; top:185px; left:245px; height:12pt; width:100%; color: #ffffff;" id="frontTRText">
		<strong id="frontTR_fonts_abc" style="{!! $input['frontTRTextFile'] !!}">{!! $input['frontTRTextFile'] !!}</strong>
	</div>
	@endif
	@if($input['frontBLFile']!='')
		<div style="z-index:2; position:absolute; top:340px; left:100px;" class="h_w_cls"id='frontBL'>
			<img src="{!! $input['frontBLFile'] !!}" class='col-12'>
		</div>
	@endif
	@if($input['frontBLTextFile']!='')
	<div class="test" style="z-index:2; position:absolute; top:420px; left:100px; height:12pt; width:100%; color: #ffffff;" id="frontBLText">
		<strong id="frontTR_fonts_abc" style="{!! $input['frontBLTextFile'] !!}">{!! $input['frontBLTextFile'] !!}</strong>
	</div>
	@endif
	@if($input['frontBRFile']!='')
		<div style="z-index:2; position:absolute; top:340px; left:257px;" class="h_w_cls"id='frontBR'>
			<img src="{!! $input['frontBRFile'] !!}" class='col-12'>
		</div>
	@endif
	@if($input['frontBRTextFile']!='')
	<div class="test" style="z-index:2; position:absolute; top:420px; left:257px; height:12pt; width:100%; color: #ffffff;" id="frontBRText">
		<strong id="frontTR_fonts_abc" style="{!! $input['frontBRTextFile'] !!}">{!! $input['frontBRTextFile'] !!}</strong>
	</div>
	@endif

	@if($input['backCenterFile']!='')
		<div style="z-index:2;position:absolute;top: 150px;left: 950px;height:150px;width:150px;" id='backCenter'>
			<img src="{!! $input['backCenterFile'] !!}" class='col-12'>
		</div>
	@endif
	@if($input['backCenterTextFile']!='')
	<div class="test" style="z-index:2; position:absolute; top:300px; left:999px; height:16px; width:100%; color: #ffffff;" id="backCenterText">
		<strong id="frontTR_fonts_abc" style="{!! $input['backCenterTextFile'] !!}">{!! $input['backCenterTextFile'] !!}</strong>
	</div>
	@endif
	<div class="col-sm-12 col-12" style="z-index: 1; cursor:pointer;">
		{!! $product[0]->image !!}
	</div>

</body>
</html>

<?php exit; //echo '<pre>';print_r($product); print_r($input); exit;?>

<script type="text/javascript">
	var svgRect = document.getElementById("Layer_2");
	<?php if($input['baseColor']!='') { ?>
		var svgBody = svgRect.getElementById("layer-1");
		svgBody.setAttribute("fill",{!! $input['baseColor'] !!});
	<?php } if($input['layer1Color']!='') { ?>
		var svgLayer1 = svgRect.getElementById("layer-2");
		svgLayer1.setAttribute("fill",{!! $input['layer1Color'] !!});
	<?php } if($input['layer2Color']!=''){ ?>
		var svgLayer2 = svgRect.getElementById("layer-3");
		svgLayer2.setAttribute("fill",{!! $input['layer2Color'] !!});
	<?php } if($input['accentColor']!='') { ?>
		var svgAccents = svgRect.getElementById("layer-4");
		svgAccents.setAttribute("fill",{!! $input['accentColor'] !!});
	<?php } ?>

	applyEmbStep1('{!! $embs !!}');

	function applyEmbStep1(file) {
		console.log(file);
		document.getElementById('embLayer').innerHTML = readFile(file);
	}
</script>