<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Kit Designer</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" type="image/png" href="{{ asset('img/casual-t-shirt-.png')}}"/>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/skins/_all-skins.min.css">

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href="http://bus-ticket.bdtask.com/bus365_demov2/assets/datatables/css/dataTables.min.css" rel="stylesheet"
          type="text/css"/>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">


</head>

<body class="skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" style="background-color: #35912e;" class="logo">
            <b>Kit Designer</b>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" style="background-color: #35912e;" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ asset('img/casual-t-shirt-.png')}}"
                                 class="user-image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"></span>
                        </a>
                    </li>
                    <li class="user user-menu">
                        <a href="#" style="background-color: #35912e;" class="btn btn-primary btn-flat"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside style="background-color: #000055;" class="main-sidebar" id="sidebar-wrapper">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img style="min-width: 50px; min-height: 50px;max-width: 50px; max-height: 50px;"
                         src="{{ asset('img/casual-t-shirt-.png')}}" class="img-circle"
                         alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Admin</a>
                </div>
            </div>
			<ul class="sidebar-menu" data-widget="tree">
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-paint-brush"></i> <span>Colors</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('colors.index') }}"><i class="fa fa-paint-brush"></i>All Colors</a></li>
                        <li><a href="{{ route('colors.create') }}"><i class="fa fa-paint-brush"></i>Add Color</a></li>
                    </ul>
                </li>
				<li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Product Category</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('product_cat.index') }}"><i class="fa fa-list"></i>All Categories</a></li>
                        <li><a href="{{ route('product_cat.create') }}"><i class="fa fa-list"></i>Add Category</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Products</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('clothes.index') }}"><i class="fa fa-list"></i>All Products</a></li>
                        <li><a href="{{ route('clothes.create') }}"><i class="fa fa-list"></i>Add Product</a></li>
                    </ul>
                </li>
				<li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Sizes</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('sizes.index')}}"><i class="fa fa-list"></i>All Sizes</a></li>
                        <li><a href="{{route('sizes.create')}}"><i class="fa fa-list"></i>Add Size</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Designs</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('designs.index')}}"><i class="fa fa-list"></i>All Designs</a></li>
                        <li><a href="{{route('designs.create')}}"><i class="fa fa-list"></i>Add Design</a></li>
                    </ul>
                </li>
				<li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Designs Layers</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('designs_layers.index')}}"><i class="fa fa-list"></i>All Designs Layers</a></li>
                        <li><a href="{{route('designs_layers.create')}}"><i class="fa fa-list"></i>Add Design Layer</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Collars</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('collars.index')}}"><i class="fa fa-list"></i>All Collars</a></li>
                        <li><a href="{{route('collars.create')}}"><i class="fa fa-list"></i>Add Collar</a></li>
                    </ul>
                </li>
				<li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Embellishments</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('embs.index') }}"><i class="fa fa-list"></i>All Embellishments</a></li>
                        <li><a href="{{ route('embs.create') }}"><i class="fa fa-list"></i>Add Embellishment</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-font"></i> <span>Fonts</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('fonts.index')}}"><i class="fa fa-list"></i>All Fonts</a></li>
                        <li><a href="{{route('fonts.create')}}"><i class="fa fa-list"></i>Add Font</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-picture-o"></i> <span>Logos</span>
                        <span class="pull-right-container">
						  <i class="fa fa-angle-left pull-right"></i>
						</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('prints.index') }}"><i class="fa fa-picture-o"></i>All Logos</a></li>
						<li><a href="{{ route('prints.create') }}"><i class="fa fa-picture-o"></i>Add Logo</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Editable Logos</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('editable_logos.index') }}"><i class="fa fa-list"></i>All Editable Logos</a></li>
                        <li><a href="{{ route('editable_logos.create') }}"><i class="fa fa-list"></i>Add Editable Logo</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Symbols</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('symbols.index') }}"><i class="fa fa-list"></i>All Symbols</a></li>
                        <li><a href="{{ route('symbols.create') }}"><i class="fa fa-list"></i>Add Symbol</a></li>
                    </ul>
                </li>
                <li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-reddit-alien"></i> <span>Alphabets</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('alphabets.index') }}"><i class="fa fa-list"></i>All Alphabets</a></li>
                        <li><a href="{{ route('alphabets.create') }}"><i class="fa fa-list"></i>Add Alphabet</a></li>
                    </ul>
                </li>
				<li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-user-secret"></i> <span>Users</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i>All Users</a></li>
                        <li><a href="{{ route('users.create') }}"><i class="fa fa-users"></i>Add User</a></li>
                    </ul>
                </li>
				<li class="treeview menu">
                    <a href="#">
                        <i class="fa fa-file-text"></i> <span>Content</span>
                        <span class="pull-right-container">
						  <i class="fa fa-angle-left pull-right"></i>
						</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('homepage_banners.index') }}"><i class="fa fa-picture-o"></i>Homepage Banners</a></li>
                        <li><a href="{{ route('text.index') }}"><i class="fa fa-file-text"></i>Homepage Text</a></li>
                        <li><a href="{{ route('teams.index') }}"><i class="fa fa-id-card-o"></i>Team Members</a></li>
                        <li><a href="{{ route('faqs.index') }}"><i class="fa fa-question-circle-o"></i>FAQs</a></li>
                        <li><a href="{{ route('coming_soon.index',['title'=>'How to Order']) }}"><i class="fa fa-first-order"></i>How to Order</a></li>
                    </ul>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>        <!-- Content Wrapper. Contains page content -->


    <style>
        #dataTable td {
            min-width: 10px;
            max-width: 50px;
            overflow: hidden;
        }
    </style>

    <div class="content-wrapper">

        <div class="container">
            <div class="row">
                @if(session('error'))
                    <div class="alert alert-danger">
                        {!! session('error') !!}
                    </div>
                @endif
				<div class="col-sm-11">
                @yield('content')
				</div>
            </div>
        </div>

    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
    </footer>
</div>
<!-- jQuery 3.1.1 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/js/adminlte.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
      rel="stylesheet">
<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script src="{{asset('frontend/js/jquery.form.js')}}"></script>

<script src="http://bus-ticket.bdtask.com/bus365_demov2/assets/datatables/js/dataTables.min.js"
        type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
//        $('#dataTable').DataTable( {
//            'columnDefs': [
//                {'max-width': '20%', 'targets': 0}
//            ],
//        } );
        $('#dataTable').DataTable({
            "pagingType": "full_numbers",
            responsive: true,
            dom: "<'row'<'col-sm-8'B><'col-sm-4'f>>tp",
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm', title: 'exportTitle'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ]
        });
    });
</script>
</body>
</html>