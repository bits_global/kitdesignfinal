      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="eyebrow-banner">
                  <span>
                     <p>CALL: 0442-23442342</p>
                  </span>
                  <span>
                     <p>Manage Orders</p>
                  </span>
                  <span>
                     <p class="country">Country</p>
                  </span>
               </div>
            </div>
         </div>
      </div>
      <!-- main banner -->
      <div class="container px-0">
         <div class="row">
            <div class="main-banner">
               <nav class="navbar navbar-expand-lg navbar-dark navbar-survival101">
                  <div class="container">
                     <a class="navbar-brand" href="{{route('home')}}">
                     <img src="{{asset('images/logo.png')}}" alt="Company" class="d-block mx-auto">
                     </a>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon">
                     <img src="{{asset('images/menu-bar.png')}}" alt="Company" class="w-100">
                     </span>
                     </button>
                     <div class="collapse navbar-collapse" id="navbarColor02">
                        <ul class="navbar-nav ">
                           <li class="nav-item">
                              <a class="nav-link<?=$productNav?>" href="{{route('product')}}">Product</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link<?=$designLabNav?>" href="{{route('configurator')}}">Design Lab</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link<?=$teamNav?>" href="{{route('our-team')}}">Our Team</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link<?=$orderNav?>" href="#">How to Order</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link<?=$faqsNav?>" href="{{route('faqs')}}">FAQs</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link<?=$contactNav?>" href="#">Get in Touch</a>
                           </li>
                           <?php  ?>
                           <li class="nav-item">
                              <a style="" class="nav-link<?=$contactNav?>"  href="{{ route('cart') }}">
                                 Cart
                                 <img style="height: 29px; width: 33px;" src="{{ asset('images/cart.png') }}">
{{--                                 @if($show)--}}
                                 <span class="badge badge-danger">
{{--                                       {{ $count }}--}}
                                    {{ Cart::count() }}

                                    </span>
{{--                                 @endif--}}
                              </a> 
                           </li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>