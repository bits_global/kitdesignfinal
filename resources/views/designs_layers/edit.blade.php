@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Design Layer Edit
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                {!! Form::model($designs_layer, ['route' => ['designs_layers.update', $designs_layer->id],'enctype'=>'multipart/form-data', 'method' => 'patch']) !!}

                <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('design_id', 'Design') !!}
                        <select class='form-control' id='design_id' name="design_id">
                            <option>Select Design</option>
                            @foreach($designs as $design)
                            <option value='{!! $design->id !!}' {{ $design->id == $designs_layer->design_id ? 'selected' : '' }} >{!! $design->name !!}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Layer Id:') !!}
                        {!! Form::text('layer_id', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Layer Title:') !!}
                        {!! Form::text('layer_title', null, ['class' => 'form-control']) !!}
                    </div>
                    
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('designs.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection