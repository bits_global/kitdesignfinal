<?php
$homeNav = '';
$productNav = '';
$designLabNav = '';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>

<!DOCTYPE html>
<html>
<head>
    @include('layouts.front_header')
    <title>KIT DESIGN - Cart</title>
    <!--  <style>
        html {
            background: #bbc3c6;
            font: 62.5%/1.5em "Trebuchet Ms", helvetica;
        }

        * {
            box-sizing: border-box;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }

        @font-face {
            font-family: 'Shopper';
            src: url('http://www.shopperfont.com/font/Shopper-Std.ttf');
        }

        .shopper {
            text-transform: lowercase;
            font: 2em 'Shopper', sans-serif;
            line-height: 0.5em;
            display: inline-block;
        }


        h1 {
            text-transform: uppercase;
            font-weight: bold;
            font-size: 2.5em;
        }

        p {
            font-size: 1.5em;
            color: #8a8a8a;
        }

        input {
            border: 0.3em solid #bbc3c6;
            padding: 0.5em 0.3em;
            font-size: 1.4em;
            color: #8a8a8a;
            text-align: center;
        }

        img {
            max-width: 9em;
            width: 100%;
            overflow: hidden;
            margin-right: 1em;
        }

        a {
            text-decoration: none;
        }

        .container {
            max-width: 75em;
            width: 95%;
            margin: 40px auto;
            overflow: hidden;
            position: relative;

            border-radius: 0.6em;
            background: #ecf0f1;
            box-shadow: 0 0.5em 0 rgba(138, 148, 152, 0.2);
        }

        .heading {
            padding: 1em;
            position: relative;
            z-index: 1;
            color: #f7f7f7;
            background: #ada5a3;
        }

        .cart {
            margin: 2.5em;
            overflow: hidden;
        }

        .cart.is-closed {
            height: 0;
            margin-top: -2.5em;
        }

        .table {
            background: #ffffff;
            border-radius: 0.6em;
            overflow: hidden;
            clear: both;
            margin-bottom: 1.8em;
        }


        .layout-inline > * {
            display: inline-block;
        }

        .align-center {
            text-align: center;
        }

        .th {
            background: #ada5a3;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
            font-size: 1.5em;
        }

        .tf {
            background: #ada5a3;
            text-transform: uppercase;
            text-align: right;
            font-size: 1.2em;
        }

        .tf p {
            color: #fff;
            font-weight: bold;
        }

        .col {
            padding: 1em;
            width: 12%;
        }

        .col-pro {
            width: 44%;
        }

        .col-pro > * {
            vertical-align: middle;
        }

        .col-qty {
            text-align: center;
            width: 17%;
        }

        .col-numeric p {
            font: bold 1.8em helvetica;
        }

        .col-total p {
            color: #12c8b1;
        }

        .tf .col {
            width: 20%;
        }

        .row > div {
            vertical-align: middle;
        }

        .row-bg2 {
            background: #f7f7f7;
        }

        .visibility-cart {
            position: absolute;
            color: #fff;
            top: 0.5em;
            right: 0.5em;
            font: bold 2em arial;
            border: 0.16em solid #fff;
            border-radius: 2.5em;
            padding: 0 0.22em 0 0.25em;
        }

        .col-qty > * {
            vertical-align: middle;
        }

        .col-qty > input {
            max-width: 2.6em;
        }


        a.qty {
            width: 1em;
            line-height: 1em;
            border-radius: 2em;
            font-size: 2.5em;
            font-weight: bold;
            text-align: center;
            background: #43ace3;
            color: #fff;
        }

        a.qty:hover {
            background: #3b9ac6;
        }

        .btn {
            padding: 10px 30px;
            border-radius: 0.3em;
            font-size: 1.4em;
            font-weight: bold;
            background: #43ace3;
            color: #fff;
            box-shadow: 0 3px 0 rgba(59, 154, 198, 1)
        }

        .btn:hover {
            box-shadow: 0 3px 0 rgba(59, 154, 198, 0)
        }

        .btn-update {
            float: right;
            margin: 0 0 1.5em 0;
        }

        .transition {
            transition: all 0.3s ease-in-out;
        }

        @media screen and ( max-width: 755px) {
            .container {
                width: 98%;
            }

            .col-pro {
                width: 35%;
            }

            .col-qty {
                width: 22%;
            }

            img {
                max-width: 100%;
                margin-bottom: 1em;
            }
        }

        @media screen and ( max-width: 591px) {

        }    </style> -->
        <style type="text/css">
            .heading{
                background-color:#eae9e9; 
                padding: 10px;
                margin-bottom: 10px;
            }
            .input_field{
                width: 35px;
                padding: 0px 0px 0px 11px;
            }
        </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css"/>
</head>

<body>
@include('layouts.front_nav')

    <div class="container">
        <div class="col-md-12" style="background-color:; padding: 8px; margin: 20px 0px;">
            <div class="text-center">
              <h2>Shopping Cart</h2>
            </div>
        </div>
        @if (Session::has('message'))
        <div class="alert alert-info" id="update_msg">{{ Session::get('message') }}</div>
        @endif
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function() {
                    $('#update_msg').fadeOut('slow');
                }, 1500);
            });
        </script>
        <div class=" cart transition is-open">

            <div class="row" style="border:1px solid;border-color: #c5c0c0;  margin: 20px 0px 40px;">
                <div class="col-md-4 heading">
                    <div class="text-center"><h5>Products</h5></div>
                </div>
                <div class="col-md-2 heading">
                    <div class="text-center"><h5>Price</h5></div>
                </div>
                <div class="col-md-2 heading">
                    <div class="text-center"><h5>Quantity</h5></div>
                </div>
                <div class="col-md-2 heading">
                    <div class="text-center"><h5>Total</h5></div>
                </div>
                <div class="col-md-2 heading">
                    <div class="text-center"><h5>Action</h5></div>
                </div>
                
                <?php foreach(Cart::content() as $row) :?>
                    <?php //echo '<pre>'; print_r($row); //exit;?>
                    <div class="col-md-4 text-center">
                         <p><?php echo $row->name; ?></p>
                    </div>
                    <div class="col-md-2 text-center">
                        <p>$<?php echo $row->price; ?></p>
                    </div>
                    <div class="col-md-2 text-center">
                        <p><?php echo $row->qty; ?></p>
                    </div>
                    <div class="col-md-2 text-center">
                       <p>$<?php echo ($row->price * $row->qty);  ?></p>
                    </div>
                    <div class="col-md-2">
                    <div class="row">                        
                        <div style="margin-left:10px;">                        
                            <form method="post" action="{{ route('cart_update', $row->rowId) }}">
                                @csrf
                                {{ method_field('PATCH') }}
                                <input type="hidden" name="qty" value="{{ $row->qty }}">
                                <button class="btn btn-update" style="background-color:#9e9999; color:white; ">+</button>
                            </form>
                        </div>
                        <div style="margin-left:5px;">                        
                            <form method="post" action="{{ route('cart_decrease', $row->rowId) }}">
                                @csrf
                                {{ method_field('PATCH') }}
                                <input type="hidden" name="qty" value="{{ $row->qty }}">
                                <button class="btn btn-update" style="background-color:#9e9999; color:white; ">-</button>
                            </form>
                        </div>
                        <div style="margin-left:5px;">                        
                            <form method="post" action="{{ route('remove', $row->rowId) }}">
                                @csrf
                                {{ method_field('PATCH') }}
                                <input type="hidden" name="qty" value="{{ $row->qty }}">
                                <button class="btn btn-update" style="background-color:#9e9999; color:white; ">Remove</button>
                            </form>
                        </div>
                    </div>
                    </div>
                <?php endforeach; ?>
                <br>
                <br>
                <br>

                @if(count(Cart::content()) > 0)
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <div class="row layout-inline">
                        <div class="col">
                            <p>Subtotal</p>
                        </div>
                        <div class="col" style="padding-left: 30px;">$<?php echo Cart::subtotal(); ?></div>
                    </div>
                    <div class="row layout-inline">
                        <div class="col">
                            <p>Tax</p>
                        </div>
                        <div class="col" style="padding-left: 30px;">$<?php echo Cart::tax(); ?></div>
                    </div>
                    <div class="row layout-inline">
                        <div class="col">
                            <p>Total</p>
                        </div>
                        <div class="col" style="padding-left: 30px;">$<?php echo Cart::total(); ?></div>
                    </div>
                </div>
                @else 
                    <div class="col-md-12 text-center">
                        <h5>Products not available in cart.</h5>
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="row">
                    <div class="col-md-5">
                        <div class="text-center" style="margin: 30px">
                            <a href="{{ url('design-lab')}}" class="btn btn-update" style="background-color:#337ab7; color:white;">Continue Shopping</a>
                        </div>
                    </div>

                    <div class="col-md-6 text-center" style="margin: 30px 30px 30px 60px;">
                        <?php $endcode = Cart::total();
                            $endcode = base64_encode($endcode);
                        ?>
{{--                        <form method="post" action="{{route('checkout')}}">--}}
{{--                        <form action="{{ route('send.mail') }}"  method="POST" enctype="multipart/form-data">--}}
{{--                            @csrf--}}
{{--                            <input type="hidden" name="codeview" value="{{$endcode}}">--}}
                            <style>
                                .emailForm{
                                    margin-right: 10px;
                                }
                            </style>
                            <?php
                            if (Cart::total() > 0) {
//                                echo '<input type="submit" class="btn btn-update emailForm" value="Checkout" style="background-color:#9e9999; color:white;" form="emailForm">';
                                echo '<input type="submit" class="btn btn-update" value="Download PDF" style="background-color:#9e9999; color:white;" form="PDFForm">';
                            }
                            else {
                                echo "Your Cart is Empty! <br />";
                            }
                            ?>
{{--                        </form>--}}

                        <form action="{{ route('send.mail') }}"  method="POST" enctype="multipart/form-data" style="display: none" id="emailForm">
                            @csrf
                            <input type="hidden" name="codeview" value="{{$endcode}}">
{{--                            <input type="submit" class="btn btn-update" value="Checkout" style="background-color:#9e9999; color:white;">--}}
                        </form>


                        <form action="{{ route('download.PDF') }}" style="display: none" id="PDFForm" method="POST">
                            @csrf
                            <?php
                            $data = Session::get('userInfo');
                            ?>

                            <input id="base" name="base" type="text" value="{{ $data['base'] }}">
                            <input id="overlay" name="overlay" type="text" value="{{ $data['overlay'] }}">
                            <input id="svgData" name="svgData" type="text" value="{{ $data['svgData'] }}">
                            <input id="svgDataStream" name="svgDataStream" type="text" value="{{ $data['svgDataStream'] }}">

                            <input id="baseColor" name="baseColor" type="text" value="{{ $data['baseColor'] }}">
                            <input id="layer1Color" name="layer1Color" type="text" value="{{ $data['layer1Color'] }}">
                            <input id="layer2Color" name="layer2Color" type="text" value="{{ $data['layer2Color'] }}">
                            <input id="accentColor" name="accentColor" type="text" value="{{ $data['accentColor'] }}">

                            <input id="frontTRFile" name="frontTRFile" type="text" value="{{ $data['frontTRFile'] }}">
                            <input id="frontTLFile" name="frontTLFile" type="text" value="{{ $data['frontTLFile'] }}">
                            <input id="frontBRFile" name="frontBRFile" type="text" value="{{ $data['frontBRFile'] }}">
                            <input id="frontBLFile" name="frontBLFile" type="text" value="{{ $data['frontBLFile'] }}">
                            <input id="backCenterFile" name="backCenterFile" type="text" value="{{ $data['backCenterFile'] }}">

                            <input id="frontTRTextFile" name="frontTRTextFile" type="text" value="{{ $data['frontTRTextFile'] }}">
                            <input id="frontTLTextFile" name="frontTLTextFile" type="text" value="{{ $data['frontTLTextFile'] }}">
                            <input id="frontBRTextFile" name="frontBRTextFile" type="text" value="{{ $data['frontBRTextFile'] }}">
                            <input id="frontBLTextFile" name="frontBLTextFile" type="text" value="{{ $data['frontBLTextFile'] }}">
                            <input id="backCenterTextFile" name="backCenterTextFile" type="text" value="{{ $data['embs'] }}">

                            <input id="embs" name="embs" value="embs" type="text">
                            <input id="base" name="PDF_Link" type="text" value="{{ $data['PDF_Link'] }}">
                        </form>

                    </div>
                    </div>

                </div>
            </div>                        
        </div>
    </div>

 @include('layouts.front_footer')
</body>
</html>