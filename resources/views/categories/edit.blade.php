@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Product Categories Edit
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                {!! Form::model($cat, ['route' => ['product_cat.update', $cat->id],'enctype'=>'multipart/form-data', 'method' => 'patch']) !!}

                    {{--                    @include('clothes.fields')--}}

                    {{--                    {!! Form::close() !!}--}}


                        <!-- Name Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::text('name',null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::label('image', 'File:') !!}
                            {!! Form::file('image',null, ['class' => 'form-control']) !!}
                        </div>


                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('product_cat.index') !!}" class="btn btn-default">Cancel</a>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection