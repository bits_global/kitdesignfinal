<!DOCTYPE html>
<html>
<head>
	<title>KIT DESIGN - PDF</title>
	<?php /* due to problem with the pdf library I am converting all the px into pt values.

by using this calculator.
http://www.endmemo.com/sconvert/pixelpoint.php

by jawad
  */ ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
		.h_w_cls{
			/*
			height:60px;
			width:60px;

			height:80px;
			width:80px;
			*/
			height:60pt;
			width:60pt;
		}

		.col-12 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
		}

		.col-12
		{
			position: relative;
			width: 100%;
			min-height: 1px;
			padding-right: 11.25pt; /* 15px;*/
			padding-left: 11.25pt; /* 15px;*/
		}

		.kit-container
		{
			width:630pt; /*840px;*/
			height:357pt; /* 476px;*/
			margin:0 auto;
			position:relative;
		}
		/*.img-base{
			width:147%;
		}
		*/
		@media only screen and (max-width: 768px) {
			/*.img-base{
                width:115% !important;
            }
            */

		}

	</style>
</head>
<body>


<h2 align="center">KIT DESIGN</h2><hr>
<?php
$embs = asset("img/embellishments/") . "/" . $input['embs'];
?>
<?php /*
    <div style="width:100%;"><h1>Important data</h1>
    <p>Base : {!! $input['base'] !!}</p>
    <p>Overlay : {!! $input['overlay'] !!}</p>
    <p>Product file : {!! $product[0]->image !!}</p>
    </div>
*/ ?>

<div class="kit-container" style="	width:630pt;
			height:357pt;
			margin:0 auto;
			position:relative;">


	<h2>Customer Details</h2>
	<ul class="list-group">
		<li class="list-group-item list-group-item-success"><strong>Product Name: </strong>{{ $data['productName'] }}</li>
		<li class="list-group-item list-group-item-info"><strong>Customer Name: </strong>{{ $data['name'] }} {{ $data['last_name'] }}</li>
		<li class="list-group-item list-group-item-warning"><strong>Email: </strong> {{ $data['email'] }}</li>
		<li class="list-group-item list-group-item-danger"><strong>Phone: </strong>{{ $data['phone'] }}</li>
		<li class="list-group-item list-group-item-success"><strong>Organization: </strong>{{ $data['organization'] }}</li>
		<li class="list-group-item list-group-item-info"><strong>Quantity: </strong>{{ $data['qty'] }}</li>
		<li class="list-group-item list-group-item-warning"><strong>Country: </strong>{{ $data['country'] }}</li>
		<li class="list-group-item list-group-item-danger"><strong>City: </strong>{{ $data['city'] }}</li>
		<li class="list-group-item list-group-item-success"><strong>State: </strong>{{ $data['state'] }}</li>
		<li class="list-group-item list-group-item-info"><strong>Postal Code: </strong>{{ $data['postalcode'] }}</li>
		<li class="list-group-item list-group-item-warning"><strong>Comments: </strong>{{ $data['comments'] }}</li>
	</ul>



	<div class="row">
		@if($input['embs'] != '')
			<div class="col-12" style="z-index: 2; width: 100%; height: 100% !important; cursor:pointer; position:absolute;"
				 id="embLayer"></div>
		@endif


		<div class="img-fluid" style="z-index: 3; cursor:pointer; position:absolute;">
			<img  src="{!! $input['base'] !!}" class='col-12 img-base' style="width:630pt;height:370.5pt;">
		</div>
		<div class="img-fluid" style="z-index: 4; cursor:pointer; position:absolute;">
			<img  src="{!! $input['overlay'] !!}" class='col-12 img-base' style="width:630pt;height:370.5pt;">
		</div>
		@if($input['frontTLFile']!='')
			<div style="z-index:2; position:absolute; width:60pt;height:60pt; @if ($cloth[0]->frontTL !='')
			{!! $cloth[0]->frontTL !!}
			@else
					top:93.75pt; left:82.5pt;
			@endif " class="h_w_cls" id='frontTL'>
				<img src="{!! $input['frontTLFile'] !!}" class='col-121' style="width:100%;height:100%;">
			</div>

		@endif
		@if($input['frontTLTextFile']!='')
			<div class="test" style="z-index:2; position:absolute; width:60pt;height:60pt; @if ($cloth[0]->frontTLText !='')
			{!! $cloth[0]->frontTLText !!}
			@else
					top:153.75pt; left:82.5pt;
			@endif height:12pt; width:100%; color: #ffffff;" id="frontTLText">
				<strong id="frontTL_fonts_abc" style="{!! $input['frontTLTextFile'] !!}">{!! $input['frontTLTextFile'] !!}</strong>
			</div>
		@endif

		@if($input['frontTRFile']!='')
			<div style="z-index:2;position:absolute; width:60pt;height:60pt; @if ($cloth[0]->frontTR !='')
			{!! $cloth[0]->frontTR !!}
			@else
					top:93.75pt; left:183.75pt;
			<?php /* top: 185px;left: 215px; */?>
			@endif " class="h_w_cls" id='frontTR'>
				<img src="{!! $input['frontTRFile'] !!}" class='col-121' style="width:100%;height:100%;">
			</div>
		@endif
		@if($input['frontTRTextFile']!='')
			<div class="test" style="z-index:2; position:absolute; @if ($cloth[0]->frontTRText !='')
			{!! $cloth[0]->frontTRText !!}
			@else
					top:138.75pt; left:183.75pt;
			@endif height:12pt; width:100%; color: #ffffff;" id="frontTRText">
				<strong id="frontTR_fonts_abc" style="{!! $input['frontTRTextFile'] !!}">{!! $input['frontTRTextFile'] !!}</strong>
			</div>
		@endif
		@if($input['frontBLFile']!='')
			<div style="z-index:2; position:absolute; width:60pt;height:60pt; @if ($cloth[0]->frontBL !='')
			{!! $cloth[0]->frontBL !!}
			@else
					top:255pt; left:75pt;
			@endif " class="h_w_cls"id='frontBL'>
				<img src="{!! $input['frontBLFile'] !!}" class='col-121' style="width:100%;height:100%;">
			</div>
		@endif
		@if($input['frontBLTextFile']!='')
			<div class="test" style="z-index:2; position:absolute; @if ($cloth[0]->frontBLText !='')
			{!! $cloth[0]->frontBLText !!}
			@else
					top:315pt; left:75pt;
			@endif  height:12pt; width:100%; color: #ffffff;" id="frontBLText">
				<strong id="frontTR_fonts_abc" style="{!! $input['frontBLTextFile'] !!}">{!! $input['frontBLTextFile'] !!}</strong>
			</div>
		@endif
		@if($input['frontBRFile']!='')
			<div style="z-index:2; position:absolute; width:60pt;height:60pt; @if ($cloth[0]->frontBR !='')
			{!! $cloth[0]->frontBR !!}
			@else
					top:255pt; left:192.75pt;
			@endif " class="h_w_cls"id='frontBR' >
				<img src="{!! $input['frontBRFile'] !!}" class='col-121' style="width:100%;height:100%;">
			</div>
		@endif
		@if($input['frontBRTextFile']!='')
			<div class="test" style="z-index:2; position:absolute; @if ($cloth[0]->frontBRText !='')
			{!! $cloth[0]->frontBRText !!}
			@else
					top:315pt; left:192.75pt;
			@endif height:12pt; width:100%; color: #ffffff;" id="frontBRText">
				<strong id="frontTR_fonts_abc" style="{!! $input['frontBRTextFile'] !!}">{!! $input['frontBRTextFile'] !!}</strong>
			</div>
		@endif

		@if($input['backCenterFile']!='')
			<div style="z-index:2;position:absolute; @if ($cloth[0]->backCenter !='')
			{!! $cloth[0]->backCenter !!}
			@else
					top:67.5pt; left:425.25pt;
			<?php /* top: 150px;left: 950px; */?>
			@endif height:150px;width:150px;" id='backCenter'>
				<img src="{!! $input['backCenterFile'] !!}" class='col-121' style="width:100%;height:100%;">
			</div>
		@endif
		@if($input['backCenterTextFile']!='')
			<div class="test" style="z-index:2; position:absolute; @if ($cloth[0]->backCenterText !='')
			{!! $cloth[0]->backCenterText !!}
			@else
					top:172.5; left:425.25pt;
			<?php /* top:300px; left:999px; */ ?>
			@endif  height:16px; width:100%; color: #ffffff;" id="backCenterText">
				<strong id="frontTR_fonts_abc" style="{!! $input['backCenterTextFile'] !!}">{!! $input['backCenterTextFile'] !!}</strong>
			</div>
		@endif
		<div class="col-sm-12 col-12" style="z-index: 1;cursor:pointer;position: absolute;">

		<!--{!! $product[0]->image !!}-->

		<!--<img src='data:image/svg+xml;utf8,{!! $product[0]->image !!}' alt=""> -->

			<img src="data:image/svg+xml;base64,{!! $product[0]->image_enc !!}" alt="" style="width:630pt;height:370.5pt;position:relative;z-index:1;">


		</div>

	</div>
</div>

</body>
</html>


<script type="text/javascript">
	var svgRect = document.getElementById("Layer_2");
	<?php if($input['baseColor']!='') { ?>
	var svgBody = svgRect.getElementById("layer-1");
	svgBody.setAttribute("fill",{!! $input['baseColor'] !!});
	<?php } if($input['layer1Color']!='') { ?>
	var svgLayer1 = svgRect.getElementById("layer-2");
	svgLayer1.setAttribute("fill",{!! $input['layer1Color'] !!});
	<?php } if($input['layer2Color']!=''){ ?>
	var svgLayer2 = svgRect.getElementById("layer-3");
	svgLayer2.setAttribute("fill",{!! $input['layer2Color'] !!});
	<?php } if($input['accentColor']!='') { ?>
	var svgAccents = svgRect.getElementById("layer-4");
	svgAccents.setAttribute("fill",{!! $input['accentColor'] !!});
	<?php } ?>

	applyEmbStep1('{!! $embs !!}');

	function applyEmbStep1(file) {
		console.log(file);
		document.getElementById('embLayer').innerHTML = readFile(file);
	}
</script>

<?php
if($product[0]->stop==true)
{

	exit;

}

//echo '<pre>';print_r($product); print_r($input); exit;
?>
