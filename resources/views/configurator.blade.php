<?php
$homeNav = '';
$productNav = '';
$designLabNav = ' active';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>
<!doctype html>
<html lang="en">
    <style>
        input#bgopacity {
            display: inline-block;
            vertical-align: middle;
        }
    </style>
<head>
    @include('layouts.front_header')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        .slidecontainer {
            width: 100%;
        }
        /*  */
        /* .scrollbarAdded{
            overflow-y: scroll;
            height: 799px;

        } */
        .scrollbarAdded a{
        text-decoration:none;
          color:black;
    }
        /* .scrollbarAdded::-webkit-scrollbar {
	width: 10px;
	
  }
  .scrollbarAdded::-webkit-scrollbar-track {
	box-shadow: inset 0 0 5px grey; 
	border-radius: 13px;
  }
  .scrollbarAdded::-webkit-scrollbar-thumb {
	background: rgb(90 67 67 / 60%); 
	
	border-radius: 10px;
  }
  .scrollbarAdded::-webkit-scrollbar-thumb:hover {
	background: rgb(83,83,83);; 
  }  */

  /*  */
  .product-name-heading h6{
      text-align:center;
      margin-right:10px;
  }
 

        .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 15px;
            border-radius: 5px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }

        .slider:hover {
            opacity: 1;
        }

        .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: #4CAF50;
            cursor: pointer;
        }

        .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: #4CAF50;
            cursor: pointer;
        }

        svg {
            width: 136px;
            height: auto;
        }

        .half_hidden {
            max-width: 140px;
            overflow: hidden;
        }

        .model-content-7 {
            padding: 0px !important;
            width: 100% !important;
            border: none;
            border-radius: 0px;
            -webkit-box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
        }

        .mymodal7 {
            position: absolute;
            z-index: 1000;
            max-width: 23%;
            left: -128px;
            top: 60px;
        }

        .regular-checkbox input[type=checkbox] + label::after {
            content: "\2713";
            color: #000;
            text-align: center;
            opacity: 0;
            position: absolute;
            left: 2px;
            transform: rotate(45deg);
            font-weight: bold;
        }

        .regular-checkbox input[type=checkbox]:checked + label {
            transform: rotate(-45deg);
            /* Testing purpose */
            cursor: pointer;
            /* doesnt work like supposed to */
        }

        .regular-checkbox input[type=checkbox]:checked + label::after {
            opacity: 1;
        }

        .picker-7 {
            background-color: #3399cc;
            color: rgb(0, 0, 0);
            border-radius: 5px;
            /* width: 36px; */
            height: 36px;
            cursor: pointer;
            -webkit-transition: all linear .2s;
            -moz-transition: all linear .2s;
            -ms-transition: all linear .2s;
            -o-transition: all linear .2s;
            transition: all linear .2s;
            border: thin solid #eee;
        }

        .picker-6 {
            background-color: #333366;
            color: rgb(255, 0, 0);
            border-radius: 5px;
            /* width: 36px; */
            height: 36px;
            cursor: pointer;
            -webkit-transition: all linear .2s;
            -moz-transition: all linear .2s;
            -ms-transition: all linear .2s;
            -o-transition: all linear .2s;
            transition: all linear .2s;
            border: thin solid #eee;
        }

        .picker-5 {
            background-color: ff9900;
            color: rgb(255, 255, 255);
            border-radius: 5px;
            /* width: 36px; */
            height: 36px;
            cursor: pointer;
            -webkit-transition: all linear .2s;
            -moz-transition: all linear .2s;
            -ms-transition: all linear .2s;
            -o-transition: all linear .2s;
            transition: all linear .2s;
            border: thin solid #eee;
        }

        .picker-4 {
            background-color: #ff9900;
            color: rgb(255, 0, 0);
            border-radius: 5px;
            /* width: 36px; */
            height: 36px;
            cursor: pointer;
            -webkit-transition: all linear .2s;
            -moz-transition: all linear .2s;
            -ms-transition: all linear .2s;
            -o-transition: all linear .2s;
            transition: all linear .2s;
            border: thin solid #eee;
        }

        .demo3 .regular-checkbox:after {
            content: '+';
        }

        .current-bg-1 {
            background-image: url(../images/StepBox-Navy.png);
            background-repeat: no-repeat;
            color: white;
            padding-top: 10px;
            padding-bottom: 20px;
            height: 40px;
            display: inline-block;
        }

        .current-bg-2 {
            background-image: url(../images/StepBox-Navy.png);
            background-repeat: no-repeat;
            color: white;
            padding-top: 10px;
            padding-bottom: 20px;
            height: 40px;
            display: inline-block;
        }

        .current-bg-3 {
            background-image: url(../images/StepBox-Navy-End.png);
            background-repeat: no-repeat;
            color: white;
            padding-top: 10px;
            padding-bottom: 20px;
            height: 40px;
            display: inline-block;
        }

        .loading_box {
            background-image: url(../public/images/loading.gif);
            background-position: center center;
            background-repeat: no-repeat;
            z-index: 50;
            height: 128px;
            width: 128px;
            top: 40%;
            left: 50%;
            border-radius: 10px;
            position: fixed;
            display: none;
        }


    </style>
</head>
<body>
@include('layouts.front_nav')
<div class="container" style="overflow:hidden;">
    <!-- Haeder -->
    <header class="header" id="header">
        <div class="row">
            <div class="col-12 col-sm-5">
                <h1 class="mrg" style="margin-bottom: 0px;">KIT DESIGNER</h1></div>
            <div class="col-7 d-none d-sm-block">
                <ul class="process-step">
                    <li style="z-index:3;" class="">Step 1<span>Select Design</span></li>
                    <li style="z-index:2;" class="">Step 2<span>Edit Design</span></li>
                    <li style="z-index:1;" class="">Step 3<span>Completed Design</span></li>
                </ul>
            </div>
        </div>
    </header>
    <!-- Haeder -->
    <main>
        <div class="row">

            <!-- Left Panel -->
            <div class="col-12 col-sm-3">
                <div class="container"><br>
                    <div class="row">
                        <h5>1 SELECT YOURS COLORS:</h5>
                    </div>

                    <div class="row add-color-1">
                        <div id='PColor' class="col-1 picker-7">
                            <input type="hidden" id="base" value="000000">
                        </div>
                        <div class="col-9">
                            <div class=""></div>
                            <p class="selected-color">PRIMARY</p>
                        </div>
                    </div>

                    <div class="row add-color-2">
                        <div id="SColor" class="col-1 picker-6"><input type="hidden" id="layer1" value="FF0000">
                        </div>
                        <div class="col-9">
                            <p class="selected-color">SECONDARY</p>
                        </div>
                    </div>

                    <div class="row add-color-3">
                        <div id="TColor" class="col-1 picker-5"><input type="hidden" id="layer2" value="FFFFFF">
                        </div>
                        <div class="col-9">
                            <p class="selected-color">TERTIARY</p>
                        </div>
                    </div>

                    <div class="row add-color-4">
                        <div id="AColor" class="col-1 picker-4"><input type="hidden" id="layer3" value="FF0000">
                        </div>
                        <div class="col-9">
                            <p class="selected-color">ACCENT</p>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <button id="myBtn" class="btn"> SHUFFULE COLOURS</button>
                    </div>

                    <br>
                    <div class="row">
                        <h5>EMBELISHMENT:</h5>
                        <p>SELECT EMBELISMENT</p>
                    </div>

                    <div class="row">
                        <div class="loading_box" id="loading_box"></div>

                        <div class="col-3 companies-logo">
                            <a href="javascript:void(0);" onclick="applyEmb('none','0')">
                                <img src="{{asset('img/embellishments/thumbs/')}}/blank.png" class="img-thumbnail embelishment" style="margin-left:0px !important ">
                            </a>
                        </div>

                        @foreach($embs as $emb)
                            <div class="col-3 companies-logo">
                                <a href="javascript:void(0);" onclick="applyEmb('{{asset('img/embellishments/')}}/{!! $emb->file !!}','{!! $emb->id !!}')">
                                    <img src="{{asset('img/embellishments/thumbs/')}}/{!! $emb->thumb !!}" class="img-thumbnail embelishment" style="margin-left:0px !important ">
                                </a>
                            </div>
                        @endforeach
                    </div>

                    <div class='row'>
                        <div class="slidecontainer">
                            <label>Opacity:</label>
                            <input type="range" name="bgopacity" id="bgopacity" value="50" min="0" max="100" step="1" onchange="rangevalue.value=value">
                            <output id="rangevalue">50</output>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <h5>LOOKING FOR INSPIRATION?</h5>
                        <p>OUR DESIGNER HAVE PUT TOGATHER
                            SOME GESIGN CONCEPT YOU CAN USE
                            AS A STARTING POINT FOR YOUR UNIQUE
                            KIT.</p>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                        <!--<button class="btn" style="float: right;"> DESIGN IDEAS </button>-->
                    </div>
                </div>
            </div>
            <!-- Left Panel End -->

            <div class="col-12 col-sm-9">

                <!-- Modal7 -->
                <div class="mymodal7" id="mymodal7" style="display: none">
                    <div class="modal-dialog">
                        <div class="modal-content model-content-7">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h6 class="modal-title modal-color">Primary Color</h6>
                                <button type="button" id="close2" data-dismiss="modal">×</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="one-one" style="">
                                    <div class="container">
                                        <div class="row mt-3">
                                            <div class="demo3 mt-2" id="checkboxes">
                                                <?php $j = 5;?>
                                                @foreach($colors as $color)
                                                    <input type="checkbox" id="checkbox-2-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="checkbox1"
                                                           onclick="changeColor('{!! $color->color !!}','base')">
                                                    <label for="checkbox-2-<?=$j?>"
                                                           style="background-color: {!! $color->color !!} !important;"></label>
                                                    <?php $j++;?>
                                                @endforeach

                                                <p>Fluoresent Colors</p>
                                                <input type="checkbox" id="checkbox-2-1"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e22687','base')">
                                                <label for="checkbox-2-1"
                                                       style="background-color: #e22687 !important;"></label>
                                                <input type="checkbox" id="checkbox-2-2"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e9e61f','base')">
                                                <label for="checkbox-2-2"
                                                       style="background-color: #e9e61f !important;"></label>
                                                <input type="checkbox" id="checkbox-2-3"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f59932','base')">
                                                <label for="checkbox-2-3"
                                                       style="background-color: #f59932 !important;"></label>
                                                <input type="checkbox" id="checkbox-2-4"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f06522','base')">
                                                <label for="checkbox-2-4"
                                                       style="background-color: #f06522 !important;"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal7 End -->

                <!-- Modal8 -->
                <div class="mymodal7" id="mymodal8" style="display: none">
                    <div class="modal-dialog">
                        <div class="modal-content model-content-7">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h6 class="modal-title modal-color">Second Color</h6>
                                <button type="button" id="close3" data-dismiss="modal">×</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="one-one" style="">
                                    <div class="container">
                                        <div class="row mt-3">
                                            <div class="demo3 mt-2" id="checkboxes">
                                                <?php $j = 5;?>
                                                @foreach($colors as $color)
                                                    <input type="checkbox" id="checkbox-3-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="checkbox1"
                                                           onclick="changeColor('{!! $color->color !!}','layer1')">
                                                    <label for="checkbox-3-<?=$j?>"
                                                           style="background-color: {!! $color->color !!} !important;"></label>
                                                    <?php $j++;?>
                                                @endforeach

                                                <p>Fluoresent Colors</p>
                                                <input type="checkbox" id="checkbox-3-1"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e22687','layer1')">
                                                <label for="checkbox-3-1"
                                                       style="background-color: #e22687 !important;"></label>
                                                <input type="checkbox" id="checkbox-3-2"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e9e61f','layer1')">
                                                <label for="checkbox-3-2"
                                                       style="background-color: #e9e61f !important;"></label>
                                                <input type="checkbox" id="checkbox-3-3"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f59932','layer1')">
                                                <label for="checkbox-3-3"
                                                       style="background-color: #f59932 !important;"></label>
                                                <input type="checkbox" id="checkbox-3-4"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f06522','layer1')">
                                                <label for="checkbox-3-4"
                                                       style="background-color: #f06522 !important;"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal8 End -->

                <!-- Modal9 -->
                <div class="mymodal7" id="mymodal9" style="display:none;">
                    <div class="modal-dialog">
                        <div class="modal-content model-content-7">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h6 class="modal-title modal-color">Terriary Color</h6>
                                <button type="button" id="close4" data-dismiss="modal">×</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="one-one" style="">
                                    <div class="container">
                                        <div class="row mt-3">
                                            <div class="demo3 mt-2" id="checkboxes">
                                                <?php $j = 5;?>
                                                @foreach($colors as $color)
                                                    <input type="checkbox" id="checkbox-4-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="checkbox1"
                                                           onclick="changeColor('{!! $color->color !!}','layer2')">
                                                    <label for="checkbox-4-<?=$j?>"
                                                           style="background-color: {!! $color->color !!} !important;"></label>
                                                    <?php $j++;?>
                                                @endforeach

                                                <p>Fluoresent Colors</p>
                                                <input type="checkbox" id="checkbox-4-1"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e22687','layer2')">
                                                <label for="checkbox-4-1"
                                                       style="background-color: #e22687 !important;"></label>
                                                <input type="checkbox" id="checkbox-4-2"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e9e61f','layer2')">
                                                <label for="checkbox-4-2"
                                                       style="background-color: #e9e61f !important;"></label>
                                                <input type="checkbox" id="checkbox-4-3"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f59932','layer2')">
                                                <label for="checkbox-4-3"
                                                       style="background-color: #f59932 !important;"></label>
                                                <input type="checkbox" id="checkbox-4-4"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f06522','layer2')">
                                                <label for="checkbox-4-4"
                                                       style="background-color: #f06522 !important;"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal9 End -->

                <!-- Modal10 -->
                <div class="mymodal7" id="mymodal10" style="display:none;">
                    <div class="modal-dialog">
                        <div class="modal-content model-content-7">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h6 class="modal-title modal-color">Accent Color</h6>
                                <button type="button" id="close5" data-dismiss="modal">×</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="one-one" style="">
                                    <div class="container">
                                        <div class="row mt-3">
                                            <div class="demo3 mt-2" id="checkboxes">
                                                <?php $j = 5;?>
                                                @foreach($colors as $color)
                                                    <input type="checkbox" id="checkbox-5-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="checkbox1"
                                                           onclick="changeColor('{!! $color->color !!}','layer3')">
                                                    <label for="checkbox-5-<?=$j?>"
                                                           style="background-color: {!! $color->color !!} !important;"></label>
                                                    <?php $j++;?>
                                                @endforeach

                                                <p>Fluoresent Colors</p>
                                                <input type="checkbox" id="checkbox-5-1"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e22687','layer3')">
                                                <label for="checkbox-5-1"
                                                       style="background-color: #e22687 !important;"></label>
                                                <input type="checkbox" id="checkbox-5-2"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#e9e61f','layer3')">
                                                <label for="checkbox-5-2"
                                                       style="background-color: #e9e61f !important;"></label>
                                                <input type="checkbox" id="checkbox-5-3"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f59932','layer3')">
                                                <label for="checkbox-5-3"
                                                       style="background-color: #f59932 !important;"></label>
                                                <input type="checkbox" id="checkbox-5-4"
                                                       class="regular-checkbox big-checkbox" value="checkbox2"
                                                       onclick="changeColor('#f06522','layer3')">
                                                <label for="checkbox-5-4"
                                                       style="background-color: #f06522 !important;"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal10 End -->

                <!-- Center Content -->
                <div class="container scrollbarAdded"><br>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <h5>2 SELECT YOUR DESIGN:</h5>
                        </div>
                        <!-- <div class="row">

                        </div> -->
                        <?php
                        $class = '';
                        ?>
                        @if(count($products) == 1)
                            @foreach($products as $product)
                               <?php
                                  $cloth = \App\Http\Controllers\ConfiguratorController::getCloth($product->prod_id);
                               ?>
                                <div class="col-sm-3">
                                    <div class="col-sm-12">

                                        <a id="productLink-{!! $product->prod_id !!}" href="{!! route('step1-addColor',['designID'=>$product->id,'productName'=>$product->name]) !!}" class="productLink">

                                            <div style="z-index: 2; position: absolute; overflow: hidden; height: 140px;" class="col-8" id="emb"></div>

                                            <div style="z-index: 3; position: absolute; overflow: hidden; height: 140px;     width: 14px;">
                                                <img src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->overlay !!}_overlay.png"
                                                     style="height: auto; width: 100%;">
                                            </div>

                                            <div style="z-index: 3; position: absolute; overflow: hidden; height: 148px;" class="col-10"><img src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->base !!}_base.png" style="height: 96%; width: 236%;"></div>

                                            <div class="col-sm-12">
                                                <div style="height: 160px !important">{!! $product->image !!}</div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 product-name-heading-2">
                                                <h6 style="text-align: center;margin-left: 15%;">{!! $product->name !!}</h6><br>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                        @elseif(count($products) > 1)
                            @foreach($products as $product)
                               <?php
                                  $cloth = \App\Http\Controllers\ConfiguratorController::getCloth($product->prod_id);
                               ?>
                                <div class="col-6 col-sm-3">
                                <a id="productLink-{!! $product->prod_id !!}" href="{!! route('step1-addColor',['designID'=>$product->id,'productName'=>$product->name]) !!}" class="productLink">

                                    <div style="z-index: 2; position: absolute; overflow: hidden; height: 140px;" class="col-9" id="emb"></div>

                                    <div style="z-index: 3; position: absolute; overflow: hidden; height: 140px;" class="col-9">
                                        <img src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->overlay !!}_overlay.png" style="height: 140px; width: 286px;">
                                    </div>

                                    <div style="z-index: 3; position: absolute; overflow: hidden; height: 140px;" class="col-9">
                                        <img src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->base !!}_base.png" style="height: 140px; width: 272px;">
                                    </div>

                                    <div class="col-sm-12 col-12" style="z-index: 1; cursor:pointer;padding-bottom:5px;">{!! $product->image !!}</div>

                                    <div class="<?php /* col-md-12 col-sm-12 */ ?> product-name-heading">
                                        <h6>{!! $product->name !!}</h6><br>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        @endif

                    </div>
                </div>
                <!-- Center Content End -->

            </div>
        </div>
    </main>

</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="{{asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('js/configuratorScript.js')}}"></script>
<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    /*// Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }
    */
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    myBtn.onclick = function () {
        var color = $("#PColor").css("background-color");
        var color1 = $("#SColor").css("background-color");
        var color2 = $("#TColor").css("background-color");
        var color3 = $("#AColor").css("background-color");

        $("#PColor").css("color", color3);
        $("#PColor").css("background-color", color3);
        changeBaseColor(color3);
        console.log(color3);

        $("#SColor").css("color", color);
        $("#SColor").css("background-color", color);
        changeLayer1Color(color);
        console.log(color);

        $("#TColor").css("color", color1);
        $("#TColor").css("background-color", color1);
        changeLayer2Color(color1);
        console.log(color1);

        $("#AColor").css("color", color2);
        $("#AColor").css("background-color", color2);
        changeLayer3Color(color2);
        console.log(color2);
    }
</script>
<script src="{{asset('frontend/color-picker/src/colorPick.js')}}"></script>
<script>
    $(".picker").colorPick({
        'initialColor': '{!! $colors[0]->color !!}',
        <?php
            $count = count($colors);
            $i = 0;
            echo "'palette': [";
            foreach ($colors as $color) {
                echo '"' . $color->color . '"';
                $i++;
                if ($i < $count)
                    echo ", ";
            }
            echo "],";
            ?>
        'onColorSelected': function () {
            console.log("The user has selected the color: " + this.color)
            this.element.css({'backgroundColor': this.color, 'color': this.color});
        }
    });

</script>
<script>
    $(document).ready(function () {
        jQuery('.add-color-1').click(function () {
            $("#mymodal8").hide();
            $("#mymodal9").hide();
            $("#mymodal10").hide();
            jQuery('#mymodal7').toggle();
        });
        $("#close2").click(function () {
            $("#mymodal7").hide();
        });
        jQuery('.add-color-2').click(function () {
            $("#mymodal7").hide();
            $("#mymodal9").hide();
            $("#mymodal10").hide();
            jQuery('#mymodal8').toggle();
        });
        $("#close3").click(function () {
            $("#mymodal8").hide();
        });
        jQuery('.add-color-3').click(function () {
            $("#mymodal7").hide();
            $("#mymodal8").hide();
            $("#mymodal10").hide();
            jQuery('#mymodal9').toggle();
        });
        $("#close4").click(function () {
            $("#mymodal9").hide();
        });
        jQuery('.add-color-4').click(function () {
            $("#mymodal7").hide();
            $("#mymodal8").hide();
            $("#mymodal9").hide();
            jQuery('#mymodal10').toggle();
        });
        $("#close5").click(function () {
            $("#mymodal10").hide();
        });
    });
</script>
<script>
    var closemodal = document.getElementById('myModal7');
    var closemodal2 = document.getElementById('myModal3');
    var closemodal3 = document.getElementById('myModal4');
    var closemodal4 = document.getElementById('myModal5');
    var closemodal5 = document.getElementById('myModal1-1');
    var closemodal6 = document.getElementById('myModal1-2');
    var closemodal7 = document.getElementById('myModal1-3');
    var closemodal8 = document.getElementById('myModal1-4');
    var closemodal9 = document.getElementById('myModal1-5');

    window.onclick = function (event) {
        if (event.target == closemodal) {
            closemodal.style.display = "none";
        }
        if (event.target == closemodal2) {
            closemodal2.style.display = "none";
        }
        if (event.target == closemodal3) {
            closemodal3.style.display = "none";
        }
        if (event.target == closemodal4) {
            closemodal4.style.display = "none";
        }
        if (event.target == closemodal5) {
            closemodal5.style.display = "none";
        }
        if (event.target == closemodal6) {
            closemodal6.style.display = "none";
        }
        if (event.target == closemodal7) {
            closemodal7.style.display = "none";
        }
        if (event.target == closemodal8) {
            closemodal8.style.display = "none";
        }
        if (event.target == closemodal9) {
            closemodal9.style.display = "none";
        }
    }
</script>
<script>
    var mySVG = document.getElementsByTagName('svg');

    for (i = 0; i < mySVG.length; i++)
        mySVG[i].setAttribute("viewBox", "0 0 600 600");
</script>
<script>
    function myTrim(x) {
        return x.replace('#', '');
    }

    function appendAnchor() {
        //var a = document.querySelectorAll("[id='productLink']");
        var a = document.querySelectorAll('.productLink');


        var href = '';
        var base = document.getElementById("base").value;
        var layer1 = document.getElementById("layer1").value;
        var layer2 = document.getElementById("layer2").value;
        var layer3 = document.getElementById("layer3").value;
        var opacity = document.getElementById("myRange").value;
        for (var i = 0; i < a.length; i++) {
            href = a[i].getAttribute("href") + "&base=" + myTrim(base) + "&layer1=" + myTrim(layer1) + "&layer2=" + myTrim(layer2) + "&layer3=" + myTrim(layer3) + "&opacity=" + myTrim(opacity);
            console.log(href);
            a[i].setAttribute("href", href);
        }
    }

    function changeColor(color, layer) {
        if (layer == 'base') {
            changeBaseColor(color);
            document.getElementById("PColor").style = "background-color: " + color;
            document.getElementById("base").value = color;
        } else if (layer == 'layer1') {
            changeLayer1Color(color);
            document.getElementById("SColor").style = "background-color: " + color;
            document.getElementById("layer1").value = color;
        } else if (layer == 'layer2') {
            changeLayer2Color(color);
            document.getElementById("TColor").style = "background-color: " + color;
            document.getElementById("layer2").value = color;
        } else if (layer == 'layer3') {
            changeLayer3Color(color);
            document.getElementById("AColor").style = "background-color: " + color;
            document.getElementById("layer3").value = color;
        }
        appendAnchor();
    }

    //appendAnchor();
</script>
<script>
    $('input[type="checkbox"]').on('change', function () {
        $('input[type="checkbox"]').not(this).prop('checked', false);
    });
</script>
<script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value + "%";
    var embs1 = document.querySelectorAll("[id='emb']");
    for (var i = 0; i < embs1.length; i++) {
        embs1[i].style.opacity = parseFloat(slider.value / 100).toFixed(2);
    }
    slider.oninput = function () {
        output.innerHTML = this.value + "%";
        for (var i = 0; i < embs1.length; i++) {
            embs1[i].style.opacity = parseFloat(this.value / 100).toFixed(2);
        }

        appendAnchor();

    }
</script>
<script>
  $('.productLink div > img').addClass("pattern-active");
</script>
<script>
    // Opacity Slider
$('#bgopacity').on('change', function (value) {
    $('#emb > img').css({
        opacity: $(this).val() * '.01'
    });
});
</script>
@include('layouts.front_footer')
</body>
@include('layouts.front_footerScript')
</html>