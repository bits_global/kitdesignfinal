<?php
$homeNav = '';
$productNav = '';
$designLabNav = '';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>
<!DOCTYPE html>
<html lang="en">

   <head>
      @include('layouts.front_header')
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css"/>
   </head>

<body>
	@include('layouts.front_nav')
	<!-- end navbar container -->
      <div class="container px-0">
         <!--Carosel slider -->
         <div class="row">
            <div class="col-md-12">
               <div id="demo" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ul class="carousel-indicators">
                     <?php
                         $class = '';
                         $i=0;
                         foreach($banners as $Banner)
                         {
                           if($i==0)
                               $class = ' class="active"';
                           else
                               $class = '';
                     ?>
					    <li data-target="#demo" data-slide-to="<?=$i?>" <?=$class?>></li>
					 <?php
                        $i++;
                           }
				     ?>
                  </ul>
                  <!-- The slideshow -->
                  <div class="carousel-inner">
				  <?php
				  $class = '';
				  $i=0;
				  foreach($banners as $Banner)
				  {
					if($i==0)
						$class = ' active';
					else
						$class = '';
					@$i++;
					?>
                    <div class="carousel-item<?=$class?>">
                        <img src="{{ asset('img/banners/') }}/{!! $Banner->image !!}" alt="" class="img-fluid" max-width="1100" height="800">
                    </div>
					<?php
                  }
				  ?>
				  </div>
                  <!-- Left and right controls -->
                  <a class="carousel-control-prev" href="#demo" data-slide="prev">
                  <img src="{{asset('images/arrow-right.png')}}" alt="arrow-right">
                  </a>
                  <a class="carousel-control-next" href="#demo" data-slide="next">
                  <img src="{{asset('images/arrow-left.png')}}" alt="arrow-left">
                  </a>
               </div>
            </div>
         </div>
      </div>
      <!------->
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="new-arrivals">
                  <h2 class="text-center pt-4">New Arrivals</h2>
               </div>
            </div>
            <div class="row pt-5">
               <div class="col-md-3">
                  <div class="new-arrivals-price">
                     <h2>PRICE</h2>
                     <p>Lorem Ipsum is simply <br> dummy text </p>
                  </div>
               </div>
               <div class="col-md-6">
                  <div id="demo" class="carousel slide" data-ride="carousel">
                     <!-- Indicators -->
                     <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                     </ul>
                     <!-- The slideshow -->
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                           <img class="kit-design-change-image img-fluid" src="images/product-1 (1).jpg">
                        </div>
                     </div>
                     <!-- Left and right controls-->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                          <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                          <span class="carousel-control-next-icon"></span>
                        </a> 
                  </div>
                  <!--<div class="img-wrapper-shirt">
                     <img src="images/shirt.png" alt="shirt" class="d-block mx-auto">
                     </div>-->
               </div>
               <div class="col-md-3  new-arrivals-price">
                  <h2>DETAILS</h2>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                     when an unknown printer took a galley of type and scrambled
                  </p>
                  <button type="button" class="btn btn-light customize-design-btn"><a href="customization.html">Customize Design <img class="color_wheel"src="images/color_wheel.png"></a></button>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <!-- Slider  -->
         <!-- <div class="row">
            <div class="col-md-12">
               <div class="carousel-wrap bg-dark py-3">
                  <div class="owl-carousel">
                     <div class="item"><a><img src="images/product-1 (1).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (2).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (3).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (4).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (1).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (1).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (3).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (1).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (1).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (3).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (1).jpg" class="img-thumbnail embelishment"></a></div>
                     <div class="item"><a><img src="images/product-1 (3).jpg" class="img-thumbnail embelishment"></a></div>
                  </div>
               </div>
            </div>
         </div> -->
      </div>
      <!------->
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="new-arrivals">
                  <h2 class="text-center pt-4">New Arrivals</h2>
               </div>
            </div>
            <div class="row pt-5">
               <div class="col-md-3">
                  <div class="new-arrivals-price">
                     <h2>{!! $text[0]->heading !!}</h2>
                     <p>{!! $text[0]->text !!}</p>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="img-wrapper-shirt">
                     <img src="{{asset('images/shirt.png')}}" alt="shirt" class="d-block mx-auto">
                  </div>
               </div>
               <div class="col-md-3  new-arrivals-price">
                  <h2>{!! $text[1]->heading !!}</h2>
                  <p>{!! $text[1]->text !!}</p>
               </div>
            </div>
         </div>
      </div>
      <?php
	  /*
	  ?>
	  <div class="container">
         <!-- Slider  -->
         <div class="row">
            <div class="col-md-12">
               <div class="carousel-wrap bg-dark py-3">
                  <div class="owl-carousel">
                     @foreach($products as $product)
					 <div class="item"><img src="{{ asset('img/t-shirts/') }}/{!! $product->base !!}_base.png" class='col-md-12'></div>
                     @endforeach
					 <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                     <div class="item"><img src="http://placehold.it/150x150"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <?php
	  */?>
      <!--END SLIDER DIV-->
      <div class="container">
         <!--our promise -->
         <div class="row">
            <div class="col-md-12">
               <div class="our-promise text-center">
                  <h2>{!! $text[2]->heading !!}</h2>
                  <p>{!! $text[2]->text !!}</p>
               </div>
            </div>
            <div class="col-md-4">
               <div class="our-promise-sec-1">
                  <img src="{{asset('images/man-and-women.png')}}" alt="" class="d-block mx-auto">
                  <h3 class="text-center">{!! $text[3]->heading !!}</h3>
                  <p class="text-center">
                     {!! $text[3]->text !!}
                  </p>
               </div>
            </div>
            <div class="col-md-4">
               <div class="our-promise-sec-1">
                  <img src="{{asset('images/man-and-women.png')}}" alt="" class="d-block mx-auto">
                  <h3 class="text-center">{!! $text[4]->heading !!}</h3>
                  <p class="text-center">
                     {!! $text[4]->text !!}
                  </p>
               </div>
            </div>
            <div class="col-md-4">
               <div class="our-promise-sec-1">
                  <img src="{{asset('images/man-and-women.png')}}" alt="" class="d-block mx-auto">
                  <h3 class="text-center">{!! $text[5]->heading !!}</h3>
                  <p class="text-center">
                     {!! $text[5]->text !!}
                  </p>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <!--our team -->
         <div class="row">
            <div class="our-team">
               <div class="col-md-12">
                  <h2 class="text-center">OUR TEAM</h2>
               </div>
            </div>
            <div class="container">
               <div class="row">
			   <?php
			   foreach($team as $teamMember)
			   {
			   ?>
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-6">
                           <div class="border-right-our-team">
                              <img src="{{ asset('img/team/') }}/{!! $teamMember->image !!}" alt="" class="img-fluid circle-shape">
                           </div>
                        </div>
                        <div class="col-6">
                           <div class="our-team-sec">
                              <h4>{!! $teamMember->name !!}</h4>
                              <p class="team-discription-text">
                                 {!! $teamMember->description !!}
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
				<?php
			    }
			    ?>
               </div>
            </div>
         </div>
      </div>
      @include('layouts.front_footer')
      
	</body>
   @include('layouts.front_footerScript')
   <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".embelishment").each(function(){
      jQuery(this).click(function(){
      var firstimage= jQuery('.kit-design-change-image').attr("src");
      var secoundimage= jQuery(this).attr("src");

      jQuery('.kit-design-change-image').attr("src" , secoundimage);

      });
      });
      });
   </script>
   <script type="text/javascript">
      $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      navText: [
       "<img src='images/owl-lef-arrow.png' class='owl-arrow-1'>",
       "<img src='images/owl-right-arrow.png' class='owl-arrow-1'>"
      ],
      autoplay: true,
      autoplayHoverPause: true,
      responsive: {
       0: {
         items: 1
       },
       600: {
         items: 3
       },
       1000: {
         items: 5
       }
      }
      })
   </script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>
   <script src="https://kit.fontawesome.com/dd4338fa1b.js"></script>
</html>