<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12 image">
    {!! Form::label('image', 'Photo:') !!}
    {!! Form::text('image', null, ['class' => 'form-control image']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('css', 'Logo CSS:') !!}
    {!! Form::textarea('css', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('prints.index') !!}" class="btn btn-default">Cancel</a>
</div>
