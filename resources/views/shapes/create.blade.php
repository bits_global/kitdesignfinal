@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Add Shape
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                {{--                {!! Form::open(['route' => 'colors.store']) !!}--}}

                <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Content -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('description', 'Content:') !!}
                        {!! Form::file('description', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-sm-6">
                        {!! Form::label('order', 'Order:') !!}
                        {!! Form::number('order', null, ['class' => 'form-control']) !!}
                    </div>



                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('colors.index') !!}" class="btn btn-default">Cancel</a>
                    </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
