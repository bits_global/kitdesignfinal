<style>
    .tt{
        width: 100px;
        max-width: 100px;
        max-height: 100px;
    }
</style>
<table class="table table-responsive"  id="dataTable">
    <thead>
        <tr>
			<th>Collar</th>
            <th>Name</th>
			<th>Category</th>
			<th>Action</th>
        </tr>
    </thead>
    <tbody>
	<?php use App\Http\Controllers\CollarsController;?>
    @foreach($prints as $print)
        <tr>
            <td>
                {!! $print->collar !!}
            </td>
			<td>
				{!! $print->name !!}
			</td>
			<td>
				{!! CollarsController::getCategoryName($print->category); !!}
			</td>
            <td>
                {!! Form::open(['route' => ['collars.destroy', $print->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('collars.show', [$print->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>