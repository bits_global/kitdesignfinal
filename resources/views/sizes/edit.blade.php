@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Size Edit
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                {!! Form::model($size, ['route' => ['sizes.update', $size->id], 'method' => 'patch']) !!}

                <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name',null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Image Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('shorthand', 'Shorthand') !!}
                        {!! Form::text('shorthand',null, ['class' => 'form-control']) !!}

                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('sizes.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection