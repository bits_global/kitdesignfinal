<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $homeText->id !!}</p>
</div>

<!-- Heading Field -->
<div class="form-group">
    {!! Form::label('heading', 'Heading:') !!}
    <p>{!! $homeText->heading !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $homeText->text !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated:') !!}
    <p>{!! $homeText->updated_at !!}</p>
</div>

