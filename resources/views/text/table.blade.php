<style>
    .tt{
        width: 100px;
        max-width: 100px;
        max-height: 100px;
    }
</style>
<table class="table table-responsive" id="dataTable">
    <thead>
        <tr>
			<th>Heading</th>
			<th>Text</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($textArr as $homeText)
        <tr>
            <td>
                {!! $homeText->heading !!}
            </td>
            <td>
                {!! $homeText->text !!}
            </td>
			<td>
                <div class='btn-group'>
                    <a href="{!! route('text.show', [$homeText->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('text.edit', [$homeText->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>