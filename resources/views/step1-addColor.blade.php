<?php
$homeNav = '';
$productNav = '';
$designLabNav = ' active';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>
        <!doctype html>
<html lang="en">
    <style>
        .modal1 .modal-body{
            overflow-y:auto;
            height:400px;
        }
        .modal1 .modal-body::-webkit-scrollbar{
            width:6px;
            background-color:#ccc;
        }
        .modal1 .modal-body::-webkit-scrollbar-thumb{
            background-color:#7BC143;
            border-radius:10px;
        }
        .modal1 .modal-body::-webkit-scrollbar-thumb:hover{
        background-color:#7BC143;
        border:1px solid #333333;
        }
        .modal1 .modal-body::-webkit-scrollbar-thumb:active{
        background-color:#7BC143;
        border:1px solid #333333;
        }
        .modal1 .modal-body::-webkit-scrollbar-track{
            box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3)
        }
        .rmve-col, .symbol-none{
            font-size: 12px;
            float: right;
            border: none;
            /* padding: 10px; */
            padding-right: 7px;
            padding-top: 3px;
            overflow: hidden;
            padding-bottom: 5px;
            padding-left: 5px;
        }
        .rmve-col i.fa.fa-trash, .symbol-none i.fa.fa-trash{opacity:0.5;}
        .rmve-col:focus, .symbol-none:focus{
            border:none;
            outline:none;
        }
        .item-content ol.prefixed.styled li{width:200px;}
    </style>
<head>
@include('layouts.front_header')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('frontend/inter-style/style.css')}}">
    <link href="{{asset('frontend/css/home.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('frontend/inter-style/responsive.css')}}">

    <script src="{{asset('js/configuratorScript.js')}}"></script>
    <script src="{{asset('js/jquery.form.js')}}"></script>

    <style>

        svg {
            width: 102%;
            height: auto;
        }

        .form {
            display: block;
            margin: 20px auto;
            background: #eee;
            border-radius: 10px;
            padding: 15px;
        }

        .submit-design {
            background-color: rgb(123, 193, 67);
            /* padding: 9px !important; */
            /* padding-left: 98px !important; */
            margin-left: -22px;
            padding: 10px 60px 10px 10px !important;
            color: white;
        }

        .progress {
            padding: 30px;
            position: relative;
            width: 100%;
            border: 1px solid #ddd;
            padding: 1px;
            border-radius: 3px;
        }

        .bar {
            background-color: #B4F5B4;
            width: 0%;
            height: 50px;
            border-radius: 3px;
        }

        .percent {
            font-weight: bolder;
            font-size: 1.5em;
            position: absolute;
            display: inline-block;
            top: 25%;
            left: 48%;
        }

        .card {
            background: #fff;
            padding: 3em;
            line-height: 1.5em;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        }

        [hidden] {
            display: none !important;
        }

        .current-bg-1 {
            background-image: url(../images/StepBox-light-gray.png) !important;
            background-repeat: no-repeat;
            color: white;
            padding-top: 10px;
            padding-bottom: 20px;
            height: 40px;
            display: inline-block;
        }

        .current-bg-2 {
            background-image: url(../images/StepBox-light-gray.png) !important;
            background-repeat: no-repeat;
            color: white;
            padding-top: 10px;
            padding-bottom: 20px;
            height: 40px;
            display: inline-block;
        }

        .current-bg-3 {
            background-image: url(../images/StepBox-light-gray-End.png) !important;
            background-repeat: no-repeat;
            color: white;
            padding-top: 10px;
            padding-bottom: 20px;
            height: 40px;
            display: inline-block;
        }

        li.selected {
            background-image: url(../images/akumaglass.png);
            color: white;
            transition: .5;
        }

        .on {
            stroke: red;
            stroke-width: 2;
        }

        /*STYLE*/
        ol {
            /* border-right: 1px solid rgba(78, 68, 50, 0.05); */
            float: left;
            list-style-type: decimal;
            margin: 1em;
            margin-left: 0;
            padding: 1em;
            padding-right: 2em;
        }

        ol.styled {
            counter-reset: item;
            list-style: none !important;
        }

        ol.styled li {
            background: rgba(78, 68, 50, 0.1);
            border-radius: 4px;
            /* font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; */
            font-size: 16px;
            line-height: 2;
            margin-bottom: 10px;
            width: 182px;
            cursor: pointer;
        }

        ol.styled li:before {
            /* content: '&#x25CB;' counter(item, decimal) ''; */
            /* content: '';
            /* background: #4e443c;
            border-radius: 4px; */
            color: #f7f7ef;
            font-size: 15px;
            margin: 0 5px 0 8px;
            padding: 4px 7px;
        }

        ol.styled li span {
            padding: 10px;
        }

        span.add-color {
            background-color: rgb(123, 193, 67);
            /* padding: 9px !important; */
            /* padding-left: 98px !important; */
            margin-left: -22px;
            /* padding-top: 14px !important;
            padding-bottom: 10px !important;
            padding-right: 56px !important;
            padding-left: 55px !important; */
            padding: 10px 99px 10px 10px !important;
            color: white;
        }

        span.add-design {
            background-color: rgb(123, 193, 67);
            /* padding: 9px !important; */
            /* padding-left: 98px !important; */
            margin-left: -22px;
            /* padding-top: 14px !important;
            padding-bottom: 10px !important;
            padding-right: 56px !important;
            padding-left: 55px !important; */
            padding: 10px 89px 10px 10px !important;
            color: white;
        }

        label.menu-label {

            color: #666;
            font-size: 18px;
            cursor: pointer;
            padding: 0.8em 1em 0.8em 0;
            padding-top: 0px !important;
            margin-bottom: 0px !important;
            padding-bottom: 0px !important;
        }

        label.menu-label:hover {
            color: rgb(83, 83, 83);
        }

        label.menu-label:hover:before {
            /* background: rgb(83,83,83); */
            background-color: lightgray;
        }

        label.menu-label:before {
            font-family: 'Impact';
            text-align: center;
            content: counter(li);
            border: 10px solid rgb(123, 193, 67);
            border-radius: 40px;
            display: inline-block;
            width: 60px;
            height: 60px;
            line-height: 40px;
            font-size: 24px;
            margin: 0 0.5em 0 0;
            background: white;
            color: rgb(83, 83, 83);
        }

        .css-accordion {
            list-style-type: none;
            counter-reset: li;

            /*padding: 20px;
            margin: 20px;*/
            /* width: 250px;*/

            /*---- End .accordion-item ----*/
        }

        .css-accordion .accordion-item {
            counter-increment: li;
            padding: 0;
            margin: 0;
        }

        .css-accordion .accordion-item .item-content-container {
            border-left: 10px solid rgb(123, 193, 67);
            padding: 6px 0;
            margin: -2px 0 -2px 25px;
        }

        .css-accordion .accordion-item .item-content-container .item-content {
            /* background: #eee; */
            padding: 5px auto;
            overflow: hidden;
            margin: 0 0 0 0px;
            /* border-radius: 2px;
            box-shadow: inset 0 2px 8px rgba(0, 0, 0, 0.5), 0 1px 2px rgba(255, 255, 255, 0.9); */
        }

        .css-accordion .accordion-item .item-content-container .item-content p {
            margin: 0.5em 0;
            font-size: 14px;
            text-shadow: 0 1px 1px rgba(255, 255, 255, 0.9);
        }

        .css-accordion .accordion-item input[type=radio] {
            display: none;
            /*---- End &:checked ----*/
        }

        .css-accordion .accordion-item input[type=radio] ~ .item-content-container {
            overflow: hidden;
        }

        .css-accordion .accordion-item input[type=radio] ~ .item-content-container .item-content {
            height: 0;
            transition: all 0.3s linear;
        }

        .css-accordion .accordion-item input[type=radio]:checked ~ .item-content-container {
            height: auto;
            overflow: visible;
        }

        .css-accordion .accordion-item input[type=radio]:checked ~ .item-content-container .item-content {
            height: auto;
            /* overflow-y: auto; */
            transition: all 0.5s linear;
        }

        .css-accordion .accordion-item input[type=radio]:checked + label {
            /* color: purple; */

        }

        .css-accordion .accordion-item input[type=radio]:checked + label:before {
            background: rgb(83, 83, 83);
            color: white;
        }

        .current-step-1 {
            background-image: url(../../images/StepBox-Green.png) !important;
            /* background-color:red; */
        }

        .step-container {
            height: 100vh;
            overflow: hidden;
        }

        .info {
            display: none
        }

        .model-content-7 {
            padding: 0px !important;
            width: 100% !important;
            border: none;
            border-radius: 0px;
            -webkit-box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
        }

        .mymodal7 {
            position: absolute;
            z-index: 9999;
            max-width: 23%;
            left: 2px;
            top: 60px;
        }

        @media (max-width: 800px) {
            /*.info-acord {
                display: none
            }*/
            .info {
                display: block
            }

            .mymodal7 {
                max-width: 67%;
                margin-top: -160%;
                left: 115px;
            }

            .modal1 {
                left: 35px;
                top: -519px;

                width: 110%;
            }

            .modal-front-top-right {
                width: 234% !important;
                left: 23px !important;
            }
        }

        .outline-btns {
            display: none;
        }

        li.green-tick {
            background: #0C0 !important;
            color: #fff;
        }

    </style>
</head>
<body>
@include('layouts.front_nav')
<!-- end navbar container -->
<div class="container px-0" style="overflow:hidden;">
    <!-- Haeder -->
    <header class="header" id="header">
        <div class="row no-gutters">
            <div class="col-12 col-sm-5">
                <h1 class="mrg" style="margin-bottom: 0px;">KIT DESIGNER</h1>
            </div>
            <div class="col-7 d-none d-sm-block">
                <ul class="process-step">
                    <li style="z-index:3;" class="complete" id="step-1">Step 1<span>Select Design</span></li>
                    <li style="z-index:2;" class="" id="step-2">Step 2<span>Edit Design</span></li>
                    <li style="z-index:1;" class="" id="step-3">Step 3<span>Completed Design</span></li>
                </ul>
            </div>
        </div>
    </header>
    <br>
    @if (Session::has('message'))
        <div class="alert alert-info" id="add_to_cart">{{ Session::get('message') }}</div>
@endif
<!-- @if (Session::has('linkdown'))
    <div class="alert alert-info">  <a target="_blank" href="{{ url('/cartorder/'.Session::get('linkdown').'.pdf') }}">Download Design</a></div>
        <div class="alert alert-info">  <a  href="{{ route('cart') }}">View Cart</a></div>
    @endif -->
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $('#add_to_cart').fadeOut('slow');
            }, 1500);
        });


        $(document).ready(function () {

            $("ol#layers li").click(
                function (event) {
                    event.preventDefault();
                    // console.log(event.currentTarget);
                    //alert('clicked');
                    //$(this).addClass('green-tick');


                }
            );


            $('#checkboxes input[type="checkbox"]').click(function () {


                    if ($(this).prop("checked") == true) {
                        console.log("Checkbox is checked.");

                        var layer_id = "#layer-" + $(this).data('parent-layer')
                        //alert($(this).data('parent-layer'));
                        //alert(layer_id);

                        $(layer_id).addClass('green-tick');
                        //$(this).addClass('green-tick');

                        $(layer_id).children('span').html('<i class="fas fa-clipboard-check"></i>');

                    } else if ($(this).prop("checked") == false) {
                        console.log("Checkbox is unchecked.");
                    }


                }
            );


        });
    </script>
</div>

<section style="width: 100%;">
    <div class="container step-container" style="overflow-y: hidden; height: auto; margin-bottom: 10px">
        <div class="row" style="overflow-y: hidden; height: auto; margin-bottom: 10px">
            <!--Left Column (Accordian, Price, & Desription)-->
            <div class="col-md-3 info-acord">
                <div class="row ">

                    <!---LEFT MENU START-->

                    <ol class="css-accordion user-journey">
                        <li class="accordion-item stage-1">
                            <input type="radio" name="accordion-control" id="stage-1-control" checked>

                            <label class="menu-label" for="stage-1-control">
                                <span class="add-color">Add Color</span>
                            </label>

                            <div class="item-content-container">
                                <div class="item-content">

                                    <ol id="layers" class="prefixed styled">
                                        <?php
                                            $i = 1;
                                        ?>
                                        @foreach($layers as $layer)
                                            <li class="add-color-<?=$i?> layer-id-<?php echo $layer->id.' '.$layer->layer_id;?> " data-layer-id="<?php echo $layer->id;?>" id="layer-<?php echo $layer->id;?>"><span>&#9675</span>{!! $layer->layer_title !!}
                                                <button class="rmve-col" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </li><?php $i++;?>
                                        @endforeach

                                        @if($embs != '')
                                        <li class="add-color--1"><span>&#9675</span>Embelishment</li>
                                        @endif

                                    </ol>
                                </div>
                            </div>
                        </li>
                        <li class="accordion-item stage-2">
                            <input type="radio" name="accordion-control" id="stage-2-control">
                            <label class="menu-label" for="stage-2-control"><span
                                        class="add-design" id="addDesign">Add Design</span></label>
                            <div class="item-content-container">
                                <div class="item-content">
                                    <ol class="prefixed styled">
                                        <li id="one-my-design"><span>&#9675</span>Front Top Right <button class="symbol-none"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                                        <li id="two-my-design"><span>&#9675</span>Front Top Left <button class="symbol-none"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                                        <li id="three-my-design"><span>&#9675</span>Front Bottom Right <button class="symbol-none"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                                        <li id="four-my-design"><span>&#9675</span>Front Bottom Left <button class="symbol-none"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                                        <li id="five-my-design"><span>&#9675</span>Back Center <button class="symbol-none"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                                    </ol>
                                </div>
                            </div>
                        </li>
                        <li class="accordion-item stage-3">
                            <input type="radio" name="accordion-control" id="stage-4-control">
                            <label class="menu-label" for="stage-4-control">
                                <span class="submit-design" id="saveDesign">Submit Design</span>
                            <!-- <a target="_blank" href="{{ url('/cartorder/'.Session::get('linkdown').'.pdf') }}"> -->
                                <!-- <span class="add-design">Save Design</span> -->
                                <!-- </a> -->
                            </label>
                        </li>
                    </ol>


                </div>

                <div class="row">
                    <hr class="col-12">
                    <div class="col-12" style="text-align: justify;">
                        <span>
                            <strong>Price:</strong>
                            ${!! $products[0]->price !!}
                        </span>


                        <span style="margin-left: 90px;">
                            <strong>Size:</strong>
                            <select>
                                <option>L</option>
                                <option>M</option>
                                <option>S</option>
                            </select>
                        </span>

                        <br>
                        <span>
                            <strong>Description:</strong>
                            {!! $products[0]->description !!}
                        </span>
                    </div>
                </div>

            </div>

            <!-- Right Column Shirt Canvas -->
            <div class="col-md-9" style="overflow: hidden;">
                <div class="container">
                    <br>
                    <!---------------------------Front Top Right--------------------------->
                    <div class="row">
                        <div class="col-sm-6 col-6" style="cursor:pointer;">
                            <div class="modal1 modal-front-top-right" id="myModal1-1"
                                 style="display:none; z-index: 9999;">
                                <div class="modal-dialog">
                                    <div class="modal-content model-content-1">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                            <button type="button" id="close1-1" data-dismiss="modal">×</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <div class="row no-gutters" style="margin-bottom: 5px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-5 btn-light float-left mr-1">0-9
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-6 btn-light float-left">Text
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters">
                                                <div class="col">
                                                    <button type="button"
                                                            class="btn-model-design btn-1 btn-light float-left mr-1">A-Z
                                                    </button>
                                                </div>
                                                <div class="col">
                                                    <button type="button"
                                                            class="btn-model-design btn-2 btn-light float-left">Symbol
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters" style="margin-top: 5px;margin-bottom: 5px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-3 btn-light float-left mr-1">
                                                        Editable
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-4 btn-light float-left">Upload
                                                    </button>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="one-one" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        <?php $i = 0;?>
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 1)
                                                            <div class="col-sm-3 col-3">
                                                                <a onclick="addFrontTR('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontTR')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1" role="tab"
                                                           data-toggle="tab" id="border1">
                                                           <span style="color: #99999b;"><i
                                                                       class="fas fa-circle"></i><span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#div1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #454545;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha3','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="all_alphabets" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        <?php $i = 0;?>
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 0)
                                                            <div class="col-sm-3 col-3">
                                                                <a onclick="addFrontTR('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontTR')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1" role="tab"
                                                           data-toggle="tab" id="border1">
                                                           <span style="color: #99999b;"><i
                                                                       class="fas fa-circle"></i><span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#div1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #454545;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha3','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="two-one" style="display:none;">
                                                <div class="row">
                                                    @foreach($symbols as $symbol)
                                                        <div class="col-sm-4 col-4"><a
                                                                    onclick="addFrontTR('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontTR')"><img
                                                                        src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1-1" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1-1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="three-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontTR')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontTR')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontTR')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <input
                                                            type="text" id="frontTR_text" class="col-12"
                                                            onkeyup="addText(this.value,'frontTR','text-align: center;')"
                                                            maxlength="12"></div>
                                                <div class="row">
                                                    @foreach($elogos as $elogo)
                                                        <div class="col-sm-4 col-4"><a
                                                                    onclick="addFrontTR('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontTR')"><img
                                                                        src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1-2" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1-2" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="four-one" style="display:none;">
                                                <h3>Select an image file</h3>
                                                <form class="form form-horizontal" id="custom1" method="post"
                                                      action="{{ route('step1-addColor.uploade') }}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div style="position:relative;">
                                                        <a class='btn btn-primary' href='javascript:;'>
                                                            Select file ...
                                                            <input required id="apkfile" name="myfile"
                                                                   accept="image/x-svg" type="file"
                                                                   style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                                                   name="file_source" size="40">
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <input class="form-control btn btn-warning" id="upload"
                                                           type="submit" value="Upload file to server">
                                                </form>

                                                <div style="height: 50px;" class="progress">
                                                    <div class="bar"></div>
                                                    <div class="percent">0%</div>
                                                </div>
                                            </div>

                                            <div class="six-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontTR')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontTR')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontTR')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;"><input
                                                            type="text" id="frontTR_text1" class="col-12"
                                                            onkeyup="addText(this.value,'frontTR','text-align: center;')"
                                                            maxlength="12"></div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1" role="tab"
                                                           data-toggle="tab" id="border1">
                                                           <span style="color: #99999b;"><i
                                                                       class="fas fa-circle"></i><span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#div1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #454545;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','frontTR_fonts_abc','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','frontTR_fonts_abc','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','alpha3','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6" style="cursor:pointer;">
                                <div class="modal1" id="myModal1-1-1" style="display:none; z-index: 9999;">
                                    <div class="modal-dialog">
                                        <div class="modal-content model-content-1">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h6 class="modal-title modal-color">Preview</h6>
                                                <button type="button" id="close1-1" data-dismiss="modal"
                                                        onclick="close_logo()">×
                                                </button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body" id="preview-1-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------------------------Front Top Left--------------------------->
                    <div class="row">
                        <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-2" style="display:none; z-index: 9999;">
                                <div class="modal-dialog">
                                    <div class="modal-content model-content-1">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                            <button type="button" id="close1-2" data-dismiss="modal">×</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <div class="row no-gutters" style="margin-bottom: 5px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-5 btn-light float-left mr-1">0-9
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-6 btn-light float-left">Text
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-1 btn-light float-left mr-1">A-Z
                                                    </button>

                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-2 btn-light float-left">Symbol
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters" style="margin-top:10px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-3 btn-light float-left mr-1">
                                                        Editable
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-4 btn-light float-left">Upload
                                                    </button>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="one-one" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 1)
                                                            <div class="col-sm-3 col-3"><a
                                                                        onclick="addFrontTL('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontTL')">
                                                                        </a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile2" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz2" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_alphabets" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 0)
                                                            <div class="col-sm-3 col-3"><a
                                                                        onclick="addFrontTL('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontTL')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile2" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz2" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="two-one" style="display:none;">
                                                <div class="row">
                                                    @foreach($symbols as $symbol)
                                                        <div class="col-sm-4 col-4"><a
                                                                    onclick="addFrontTL('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontTL')"><img
                                                                        src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile2-1" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz2-1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile2-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz2-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="three-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontTL')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontTL')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontTL')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <input
                                                            type="text" id="frontTL_text" class="col-12"
                                                            onkeyup="addText(this.value,'frontTL','text-align: center;')"
                                                            maxlength="12"></div>
                                                <div class="row">
                                                    @foreach($elogos as $elogo)
                                                        <div class="col-sm-4 col-4"><a
                                                                    onclick="addFrontTL('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontTL')"><img
                                                                        src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile2-2" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz2-2" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile2-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz2-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="four-one" style="display:none;">
                                                <h3>Select an image file</h3>
                                                <form class="form form-horizontal" id="custom2" method="post"
                                                      action="{{ route('step1-addColor.uploade') }}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div style="position:relative;">
                                                        <a class='btn btn-primary' href='javascript:;'>
                                                            Select file ...
                                                            <input required id="apkfile" name="myfile"
                                                                   accept="image/x-svg" type="file"
                                                                   style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                                                   name="file_source" size="40">
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <input class="form-control btn btn-warning" id="upload"
                                                           type="submit" value="Upload file to server">
                                                </form>

                                                <div style="height: 50px;" class="progress">
                                                    <div class="bar"></div>
                                                    <div class="percent">0%</div>
                                                </div>
                                            </div>
                                            <div class="six-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontTL')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontTL')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontTL')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;"><input
                                                            type="text" id="frontTL_text1" class="col-12"
                                                            onkeyup="addText(this.value,'frontTL','text-align: center;')"
                                                            maxlength="12"></div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1" role="tab"
                                                           data-toggle="tab" id="border1">
                                                           <span style="color: #99999b;"><i
                                                                       class="fas fa-circle"></i><span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#div1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #454545;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','frontTL_fonts_abc','frontTL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','frontTR_fonts_abc','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','alpha3','frontTR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6" style="cursor:pointer;">
                                <div class="modal1" id="myModal1-2-2" style="display:none; z-index: 9999;">
                                    <div class="modal-dialog">
                                        <div class="modal-content model-content-1">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h6 class="modal-title modal-color">Preview</h6>
                                                <button type="button" id="close1-1" data-dismiss="modal"
                                                        onclick="close_logo()">×
                                                </button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body" id="preview-2-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------------------------Front Bottom Right--------------------------->
                    <div class="row">
                        <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-3" style="display:none; z-index: 9999;">
                                <div class="modal-dialog">
                                    <div class="modal-content model-content-1">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                            <button type="button" id="close1-3" data-dismiss="modal">×</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <div class="row no-gutters" style="margin-bottom: 5px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-5 btn-light float-left mr-1">0-9
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-6 btn-light float-left">Text
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-1 btn-light float-left mr-1">A-Z
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-2 btn-light float-left">Symbol
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters" style="margin-top:10px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-3 btn-light float-left mr-1">
                                                        Editable
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-4 btn-light float-left">Upload
                                                    </button>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="one-one" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 1)
                                                            <div class="col-sm-3 col-3"><a
                                                                        onclick="addFrontBR('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontBR')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile3" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz3" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i></span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile3">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz3">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_alphabets" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 0)
                                                            <div class="col-sm-3 col-3"><a
                                                                        href="javascript:void(0)"
                                                                        onclick="return addFrontBR('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontBR')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile3" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz3" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i></span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile3">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz3">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="two-one" style="display:none;">
                                                <div class="row">
                                                    @foreach($symbols as $symbol)
                                                        <div class="col-sm-4 col-4"><a
                                                                    href="javascript:void(0)"
                                                                    onclick="return addFrontBR('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontBR')"><img
                                                                        src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile3-1" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz3-1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i></span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile3-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz3-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="three-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontBR')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontBR')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontBR')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px;padding-bottom: 10px;">
                                                    <input
                                                            type="text" id="frontBR_text" class="col-12"
                                                            onkeyup="addText(this.value,'frontBR','text-align: center;')"
                                                            maxlength="12"></div>
                                                <div class="row">
                                                    @foreach($elogos as $elogo)
                                                        <div class="col-sm-4 col-4"><a href="javascript:void(0)"
                                                                                       onclick="return addFrontBR('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontBR')"><img
                                                                        src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile3-2" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz3-2" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i></span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile3-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz3-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="four-one" style="display:none;">
                                                <h3>Select an image file</h3>
                                                <form class="form form-horizontal" id="custom3" method="post"
                                                      action="{{ route('step1-addColor.uploade') }}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div style="position:relative;">
                                                        <a class='btn btn-primary' href='javascript:;'>
                                                            Select file ...
                                                            <input required id="apkfile" name="myfile"
                                                                   accept="image/x-svg" type="file"
                                                                   style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                                                   name="file_source" size="40">
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <input class="form-control btn btn-warning" id="upload"
                                                           type="submit" value="Upload file to server">
                                                </form>

                                                <div style="height: 50px;" class="progress">
                                                    <div class="bar"></div>
                                                    <div class="percent">0%</div>
                                                </div>
                                            </div>
                                            <div class="six-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontBR')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->id !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontBR')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontBR')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;"><input
                                                            type="text" id="frontBR_text1" class="col-12"
                                                            onkeyup="addText(this.value,'frontBR','text-align: center;')"
                                                            maxlength="12"></div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1" role="tab"
                                                           data-toggle="tab" id="border1">
                                                           <span style="color: #99999b;"><i
                                                                       class="fas fa-circle"></i><span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#div1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #454545;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','frontBR_fonts_abc','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','frontBR_fonts_abc','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','frontBR_fonts_abc','frontBR')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6" style="cursor:pointer;">
                                <div class="modal1" id="myModal1-3-3" style="display:none; z-index: 9999;">
                                    <div class="modal-dialog">
                                        <div class="modal-content model-content-1">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h6 class="modal-title modal-color">Preview</h6>
                                                <button type="button" id="close1-1" data-dismiss="modal"
                                                        onclick="close_logo()">×
                                                </button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body" id="preview-3-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------------------------Front Bottom Left--------------------------->
                    <div class="row">
                        <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-4" style="display:none; z-index: 9999;">
                                <div class="modal-dialog">
                                    <div class="modal-content model-content-1">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                            <button type="button" id="close1-4" data-dismiss="modal">×</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <div class="row no-gutters" style="margin-bottom: 5px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-5 btn-light float-left mr-1">0-9
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-6 btn-light float-left">Text
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-1 btn-light float-left mr-1">A-Z
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-2 btn-light float-left">Symbol
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters" style="margin-top:10px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-3 btn-light float-left mr-1">
                                                        Editable
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-4 btn-light float-left">Upload
                                                    </button>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="one-one" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 1)
                                                            <div class="col-sm-3 col-3"><a href="javascript:void(0)"
                                                                                           onclick="return addFrontBL('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontBL')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile4" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz4" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile4">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz4">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_alphabets" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 0)
                                                            <div class="col-sm-3 col-3"><a href="javascript:void(0)"
                                                                                           onclick="return addFrontBL('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontBL')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile4" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz4" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile4">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz4">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="two-one" style="display:none;">
                                                <div class="row">
                                                    @foreach($symbols as $symbol)
                                                        <div class="col-sm-4 col-4"><a href="javascript:void(0)"
                                                                                       onclick="return addFrontBL('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontBL')"><img
                                                                        src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile4-1" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz4-1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile4-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz4-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="three-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontBL')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontBL')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontBL')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <input
                                                            type="text" id="frontBL_text" class="col-12"
                                                            onkeyup="addText(this.value,'frontBL','text-align: center;')"
                                                            maxlength="12"></div>
                                                <div class="row">
                                                    @foreach($elogos as $elogo)
                                                        <div class="col-sm-4 col-4"><a href="javascript:void(0)"
                                                                                       onclick="addFrontBL('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontBL')"><img
                                                                        src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <!----------------------------------------------------->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile4-2" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz4-2" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile4-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz4-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="four-one" style="display:none;">
                                                <h3>Select an image file</h3>
                                                <form class="form form-horizontal" id="custom4" method="post"
                                                      action="{{ route('step1-addColor.uploade') }}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div style="position:relative;">
                                                        <a class='btn btn-primary' href='javascript:;'>
                                                            Select file ...
                                                            <input required id="apkfile" name="myfile"
                                                                   accept="image/x-svg" type="file"
                                                                   style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                                                   name="file_source" size="40">
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <input class="form-control btn btn-warning" id="upload"
                                                           type="submit" value="Upload file to server">
                                                </form>

                                                <div style="height: 50px;" class="progress">
                                                    <div class="bar"></div>
                                                    <div class="percent">0%</div>
                                                </div>
                                            </div>
                                            <div class="six-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'frontBL')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'frontBL')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'frontBL')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;"><input
                                                            type="text" id="frontBL_text1" class="col-12"
                                                            onkeyup="addText(this.value,'frontBL','text-align: center;')"
                                                            maxlength="12"></div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1" role="tab"
                                                           data-toggle="tab" id="border1">
                                                           <span style="color: #99999b;"><i
                                                                       class="fas fa-circle"></i><span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#div1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #454545;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','frontBL_fonts_abc','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','frontBL_fonts_abc','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','frontBL_fonts_abc','frontBL')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6" style="cursor:pointer;">
                                <div class="modal1" id="myModal1-4-4" style="display:none; z-index: 9999;">
                                    <div class="modal-dialog">
                                        <div class="modal-content model-content-1">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h6 class="modal-title modal-color">Preview</h6>
                                                <button type="button" id="close1-1" data-dismiss="modal"
                                                        onclick="close_logo()">×
                                                </button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body" id="preview-4-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------------------------Back Center--------------------------->
                    <div class="row">
                        <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-5" style="display:none; z-index: 9999;">
                                <div class="modal-dialog">
                                    <div class="modal-content model-content-1">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                            <button type="button" id="close1-5" data-dismiss="modal">×</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body" id="modal1 .modal-body">
                                            <div class="row no-gutters" style="margin-bottom: 5px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-5 btn-light float-left mr-1">0-9
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-6 btn-light float-left">Text
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-1 btn-light float-left mr-1">A-Z
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-2 btn-light float-left">Symbol
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row no-gutters" style="margin-top:10px;">
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-3 btn-light float-left mr-1">
                                                        Editable
                                                    </button>
                                                </div>
                                                <div class="col-6">
                                                    <button type="button"
                                                            class="btn-model-design btn-4 btn-light float-left">Upload
                                                    </button>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="one-one" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 1)
                                                            <div class="col-sm-3 col-3"><a href="javascript:void(0)"
                                                                                           onclick="addBackCenter('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','backCenter')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile5" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz5" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile5">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz5">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_alphabets" style="display:none;">
                                                <div class="container">
                                                    <div class="row mt-3">
                                                        @foreach($alphabets as $alphabet) @if($alphabet->flag == 0)
                                                            <div class="col-sm-3 col-3"><a href="javascript:void(0)"
                                                                                           onclick="addBackCenter('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','backCenter')"><img
                                                                            src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a>
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile5" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz5" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile5">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha1','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz5">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','alpha2','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="two-one" style="display:none;">
                                                <div class="row">
                                                    @foreach($symbols as $symbol)
                                                        <div class="col-sm-4 col-4"><a href="javascript:void(0)"
                                                                                       onclick="return addBackCenter('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','backCenter')"><img
                                                                        src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile5-1" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz5-1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile5-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz5-1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="three-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'backCenter')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'backCenter')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'backCenter')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px;padding-bottom: 10px;">
                                                    <input
                                                            type="text" id="backCenter_text" class="col-12"
                                                            onkeyup="addText(this.value,'backCenter','text-align: center;')"
                                                            maxlength="12"></div>
                                                <div class="row">
                                                    @foreach($elogos as $elogo)
                                                        <div class="col-sm-4 col-4"><a href="javascript:void(0)"
                                                                                       onclick="return addBackCenter('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','backCenter')"><img
                                                                        src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile5-2" role="tab"
                                                           data-toggle="tab" id="border1"><span style="color: #99999b;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz5-2" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile5-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S1','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz5-2">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changeColor('{!! $color->color !!}','S2','backCenter')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="four-one" style="display:none;">
                                                <h3>Select an image file</h3>
                                                <form class="form form-horizontal" id="custom5" method="post"
                                                      action="{{ route('step1-addColor.uploade') }}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div style="position:relative;">
                                                        <a class='btn btn-primary' href='javascript:;'>
                                                            Select file ...
                                                            <input required id="apkfile" name="myfile"
                                                                   accept="image/x-svg" type="file"
                                                                   style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                                                   name="file_source" size="40">
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <input class="form-control btn btn-warning" id="upload"
                                                           type="submit" value="Upload file to server">
                                                </form>

                                                <div style="height: 50px;" class="progress">
                                                    <div class="bar"></div>
                                                    <div class="percent">0%</div>
                                                </div>
                                            </div>
                                            <div class="six-one" style="display:none;">
                                                <div class="row" style="padding-left: 10px; padding-right: 10px;">Text
                                                    (Max 12 characters)
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="fonts" onchange="font_change(this.value,'backCenter')"
                                                            class="col-12">
                                                        <option>Select Font</option>
                                                        @foreach($fonts as $font)
                                                            <option value="{!! $font->name !!}">{!! $font->name !!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="sizes" onchange="size_change(this.value,'backCenter')"
                                                            class="col-12">
                                                        <option>Select Sizes</option>
                                                        <option value="8px">8</option>
                                                        <option value="9px">9</option>
                                                        <option value="10px">10</option>
                                                        <option value="11px">11</option>
                                                        <option value="12px">12</option>
                                                        <option value="14px">14</option>
                                                        <option value="16px">16</option>
                                                        <option value="18px">18</option>
                                                        <option value="20px">20</option>
                                                        <option value="22px">22</option>
                                                        <option value="24px">24</option>
                                                        <option value="26px">26</option>
                                                        <option value="28px">28</option>
                                                        <option value="36px">36</option>
                                                        <option value="48px">48</option>
                                                        <option value="72px">72</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                                    <select id="text_style"
                                                            onchange="text_style_change(this.value,'backCenter')"
                                                            class="col-12">
                                                        <option>Select Text Style</option>
                                                        <option value="curve">Curve</option>
                                                        <option value="straight">Straight</option>
                                                    </select>
                                                </div>
                                                <div class="row"
                                                     style="padding-left: 10px; padding-right: 10px;padding-bottom: 10px;">
                                                    <input
                                                            type="text" id="backCenter_text1" class="col-12"
                                                            onkeyup="addText(this.value,'backCenter','text-align: center;')"
                                                            maxlength="12"></div>
                                                <hr>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile1" role="tab"
                                                           data-toggle="tab" id="border1">
                                                           <span style="color: #99999b;"><i
                                                                       class="fas fa-circle"></i><span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #040404;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#div1" role="tab" data-toggle="tab"
                                                           id="border2"><span style="color: #454545;"><i
                                                                        class="fas fa-circle"></i><span></a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active demo3 mt-2"
                                                         id="profile1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','backCenter_fonts_abc','backCenterText')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','backCenter_fonts_abc','backCenterText')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-1-<?=$i?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="checkbox1"
                                                                   onclick="changetextColor('{!! $color->color !!}','backCenter_fonts_abc','backCenterText')">
                                                            <label for="checkbox-1-<?=$i?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $i++?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6" style="cursor:pointer;">
                                <div class="modal1" id="myModal1-5-5" style="display:none; z-index: 9999;">
                                    <div class="modal-dialog">
                                        <div class="modal-content model-content-1">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h6 class="modal-title modal-color">Preview</h6>
                                                <button type="button" id="close1-1" data-dismiss="modal"
                                                        onclick="close_logo()">×
                                                </button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body" id="preview-5-5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @for($i=1; $i<=count($layers); $i++)
                        <div class="mymodal7" id="myModal<?=$i?>" style="display: none">
                            <div class="modal-dialog">
                                <div class="modal-content model-content-7">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h6 class="modal-title modal-color">{!! $layers[$i-1]->layer_title !!}
                                            Color</h6>
                                        <button type="button" id="close<?=$i?>" data-dismiss="modal">×</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="one-one" style="">
                                            <div class="container">
                                                <div class="row mt-3">
                                                    <div class="demo3 mt-2 checkbox-block" id="checkboxes">
                                                        <?php $j = 5;?>
                                                        @foreach($colors as $color)
                                                            <input type="checkbox" id="checkbox-<?=$i + 12?>-<?=$j?>"
                                                                   class="regular-checkbox big-checkbox"
                                                                   value="{!! $color->color !!}"
                                                                   onclick="changeS1Colors('{!! $color->color !!}','{!! $layers[$i-1]->layer_id !!}')"
                                                                   data-parent-layer="<?php echo $layers[$i - 1]->id;?>">
                                                            <label for="checkbox-<?=$i + 12?>-<?=$j?>"
                                                                   style="background-color: {!! $color->color !!} !important;"></label>
                                                            <?php $j++?>
                                                        @endforeach
                                                        <p>Fluoresent Colors</p>
                                                        <input type="checkbox" id="checkbox-<?=$i + 12?>-<?=$j?>"
                                                               class="regular-checkbox big-checkbox" value="#e22687"
                                                               onclick="changeS1Colors('#e22687','{!! $layers[$i-1]->layer_id !!}')"
                                                               data-parent-layer="<?php echo $layers[$i - 1]->id;?>">
                                                        <label for="checkbox-<?=$i + 12?>-<?=$j++?>"
                                                               style="background-color: #e22687 !important;"></label>
                                                        <input type="checkbox" id="checkbox-<?=$i + 12?>-<?=$j?>"
                                                               class="regular-checkbox big-checkbox" value="#e9e61f"
                                                               onclick="changeS1Colors('#e9e61f','{!! $layers[$i-1]->layer_id !!}')">
                                                        <label for="checkbox-<?=$i + 12?>-<?=$j++?>"
                                                               style="background-color: #e9e61f !important;"></label>
                                                        <input type="checkbox" id="checkbox-<?=$i + 12?>-<?=$j?>"
                                                               class="regular-checkbox big-checkbox" value="#f59932"
                                                               onclick="changeS1Colors('#f59932','{!! $layers[$i-1]->layer_id !!}')">
                                                        <label for="checkbox-<?=$i + 12?>-<?=$j++?>"
                                                               style="background-color: #f59932 !important;"></label>
                                                        <input type="checkbox" id="checkbox-<?=$i + 12?>-<?=$j?>"
                                                               class="regular-checkbox big-checkbox" value="#f06522"
                                                               onclick="changeS1Colors('#f06522','{!! $layers[$i-1]->layer_id !!}')">
                                                        <label for="checkbox-<?=$i + 12?>-<?=$j++?>"
                                                               style="background-color: #f06522 !important;"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor

                    <div class="mymodal7" id="myModal-1" style="display:none;" style="z-index:9999;">
                        <div class="modal-dialog">
                            <div class="modal-content model-content-7">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h6 class="modal-title modal-color">Embelishment Color</h6>
                                    <button type="button" id="close-1" data-dismiss="modal">×</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="one-one" style="">
                                        <div class="container">
                                            <div class="row mt-3">
                                                <div class="demo4 mt-2" id="checkboxes">
                                                    <?php $j = 5?>
                                                    @foreach($colors as $color)
                                                        <input type="checkbox" id="checkbox-6-<?=$j?>"
                                                               class="regular-checkbox big-checkbox"
                                                               value="{!! $color->color !!}"
                                                               onclick="changeEmbColor('{!! $color->color !!}')">
                                                        <label for="checkbox-6-<?=$j?>"
                                                               style="background-color: {!! $color->color !!} !important;"></label>
                                                        <?php $j++?>
                                                    @endforeach
                                                    <p>Fluoresent Colors</p>
                                                    <input type="checkbox" id="checkbox-6-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="#e22687"
                                                           onclick="changeEmbColor('#e22687')">
                                                    <label for="checkbox-6-<?=$j++?>"
                                                           style="background-color: #e22687 !important;"></label>
                                                    <input type="checkbox" id="checkbox-6-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="#e9e61f"
                                                           onclick="changeEmbColor('#e9e61f')">
                                                    <label for="checkbox-6-<?=$j++?>"
                                                           style="background-color: #e9e61f !important;"></label>
                                                    <input type="checkbox" id="checkbox-6-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="#f59932"
                                                           onclick="changeEmbColor('#f59932')">
                                                    <label for="checkbox-6-<?=$j++?>"
                                                           style="background-color: #f59932 !important;"></label>
                                                    <input type="checkbox" id="checkbox-6-<?=$j?>"
                                                           class="regular-checkbox big-checkbox" value="#f06522"
                                                           onclick="changeEmbColor('#f06522')">
                                                    <label for="checkbox-6-<?=$j++?>"
                                                           style="background-color: #f06522 !important;"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row selected-img-block">
                        <?php if($embs != '')
                        {
                        ?>
                        <div class="col-12 selected-img-holder"
                             style="z-index: 2; width: 95%; height: 90% !important; cursor:pointer; position:absolute;"
                             id="embLayer"></div>
                        <?php
                        }?>
                        <?php /**/?>
                        <div class="img-fluid selected-img-box" style="z-index: 3; cursor:pointer; position:absolute;"><img
                                    src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->base !!}_base.png" class='col-12'>
                        </div>
                        <?php /**/?>
                        <div class="img-fluid selected-img-box" style="z-index: 4; cursor:pointer; position:absolute;"><img
                                    src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->overlay !!}_overlay.png"
                                    class='col-12'></div>
                        <?php /**/?>
                        @if($collar!=0)
                            <div id='collarDiv' class="col-sm-11" style="z-index: 2; cursor:pointer; position: absolute; margin-top: -3px;">{!! $collars[0]->collar !!}</div>
                        @endif

                        <div class="col-sm-12 col-12" id="svgDataDiv" style="z-index: 1; cursor:pointer;">
                            {!! $products[0]->image !!}
                        </div>

                        <div class="col-sm-12" style="z-index: 3; background: #ffffff; padding: 12px;">
                            <div class="col-sm-2 col-3 off-on" style="padding:0px;">
                                <div class="outline-btns">
                                    <div class="col-6 m-show off-style" onclick="addOutline()">ON</div>
                                    <div class=" col-6 s-hide active-1 box-1 on-style" onclick="removeOutline()">Off
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->frontTL !='')
                             {!! $cloth[0]->frontTL !!}
                        @else
                                top:125px; left:110px;
                        @endif
                                height:80px; width:80px;" id='frontTL'>
                        </div>

                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->frontTLText !='')
                            {!! $cloth[0]->frontTLText !!}
                        @else
                                top:205px; left:110px;
                        @endif
                                height:12pt; width:100%; color: #ffffff;" id='frontTLText'>
                        </div>

                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->frontTR !='')
                                {!! $cloth[0]->frontTR !!}
                        @else
                                top:125px; left:245px;
                        @endif
                                height:80px; width:80px;" id='frontTR'>
                        </div>

                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->frontTRText !='')
                            {!! $cloth[0]->frontTRText !!}
                        @else
                                top:185px; left:245px;
                        @endif
                                height:12pt; width:100%; color: #ffffff;" id='frontTRText'>
                        </div>

                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->frontBL !='')
                            {!! $cloth[0]->frontBL !!}
                        @else
                                top:340px; left:100px;
                        @endif
                                height:80px; width:80px;" id='frontBL'>
                        </div>

                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->frontBLText !='')
                                {!! $cloth[0]->frontBLText !!}
                        @else
                                top:420px; left:100px;
                        @endif
                                height:12pt; width:100%; color: #ffffff;" id='frontBLText'>
                        </div>

                        <div class="test"
                             style="z-index:2; position:absolute;
                             @if ($cloth[0]->frontBR !='')
                                     {!! $cloth[0]->frontBR !!}
                             @else
                                     top:340px; left:257px;
                             @endif
                                     height:80px; width:80px;" id='frontBR'>
                        </div>

                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->frontBRText !='')
                                {!! $cloth[0]->frontBRText !!}
                        @else
                                top:420px; left:257px;
                        @endif
                                height:12pt; width:100%; color: #ffffff;" id='frontBRText'>
                        </div>

                        <div class="test" style="z-index:2; position:absolute;
                        @if ($cloth[0]->backCenter !='')
                                {!! $cloth[0]->backCenter !!}
                        @else
                                top:90px; left:567px;
                        @endif
                                height:150px; width:150px;" id='backCenter'>
                        </div>

                        <!--<div style="z-index:2; position:absolute; top:250px; left:600px; height:16px; width:100%; color: #ffffff;">
                                 <div class="test col-2" id='backCenterText' style="text-align: center;"></div>
                             </div>-->
                        <div style="z-index:2; position:absolute;
                        @if ($cloth[0]->backCenterText !='')
                                {!! $cloth[0]->backCenterText !!}
                        @else
                                top:230px; left:575px;
                        @endif
                                height:16px; width:100%; color: #ffffff;">
                            <div class="test col-2" id='backCenterText' style="text-align: center;"></div>
                        </div>

                    </div>

                    <!-----------------  Submit Design Modal  ------------------------>
                    <div id="myModal-kit" class="modal" style="z-index: 999; margin-top: 50px;">
                        <div class="modal-content">
                            <span class="close-kit" style="float: right;">&times;</span>

                            <form action="{{ route('addToCart') }}" method="POST" enctype="multipart/form-data" class="col-12 col-sm-12">
{{--                            <form action="{{ route('step1-addColor.download-pdf') }}" method="POST" enctype="multipart/form-data" class="col-12 col-sm-12">--}}
{{--                            <form action="{{ route('download.PDF') }}" method="POST" enctype="multipart/form-data" class="col-12 col-sm-12">--}}

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input id="base" name="base" type="hidden" value="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->base !!}_base.png">
                                    <input id="overlay" name="overlay" type="hidden" value="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->overlay !!}_overlay.png">
                                    <input id="svgData" name="svgData" type="hidden" value="{!! $_REQUEST['designID'] !!}">
                                    <input id="svgDataStream" name="svgDataStream" type="hidden" value="{{ $products[0]->image }}">

                                    <input type="hidden" name="productName" value="{{ $productName }}">
                                    <input id="baseColor" name="baseColor" type="hidden">
                                    <input id="layer1Color" name="layer1Color" type="hidden">
                                    <input id="layer2Color" name="layer2Color" type="hidden">
                                    <input id="accentColor" name="accentColor" type="hidden">

                                    <input id="frontTRFile" name="frontTRFile" type="hidden">
                                    <input id="frontTLFile" name="frontTLFile" type="hidden">
                                    <input id="frontBRFile" name="frontBRFile" type="hidden">
                                    <input id="frontBLFile" name="frontBLFile" type="hidden">
                                    <input id="backCenterFile" name="backCenterFile" type="hidden">

                                    <input id="frontTRTextFile" name="frontTRTextFile" type="hidden">
                                    <input id="frontTLTextFile" name="frontTLTextFile" type="hidden">
                                    <input id="frontBRTextFile" name="frontBRTextFile" type="hidden">
                                    <input id="frontBLTextFile" name="frontBLTextFile" type="hidden">
                                    <input id="backCenterTextFile" name="backCenterTextFile" type="hidden">

                                    <input id="embs" name="embs" value="{{ $embs ? $embs[0]->file : '' }}" type="hidden">

                                    <div class="col-12">
                                        <div class="container">
                                            <h3>Submit Design</h3>
                                            <div class="row" style="margin-top: 7%;">
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtName">Name*</label>
                                                        <input id="txtName" type='text' name="name" required="">
                                                    </fieldset>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtlname">Last Name*</label>
                                                        <input id="txtlname" type='text' name="last_name" required="">
                                                    </fieldset>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtemail">Email*</label>
                                                        <input id="txtemail" type='email' name="email" required="">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtnumber">Phone*</label>
                                                        <input id="txtnumber" type='number' name="phone" required="">
                                                    </fieldset>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtorganization">Organization/School/Club</label>
                                                        <input id="txtorganization" type='text' name="organization" required="">
                                                    </fieldset>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtqt">Quantity</label>
                                                        <input id="txtqt" type='number' name="qty" required="">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtcountry">Country</label>
                                                        <input id="txtcountry" type='text' name="country" required="">
                                                    </fieldset>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtcity">City</label>
                                                        <input id="txtcity" type='text' name="city" required="">
                                                    </fieldset>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtstate">State</label>
                                                        <input id="txtstate" type='text' name="state" required="">
                                                    </fieldset>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtPcode">Postal Code</label>
                                                        <input id="txtPcode" type='text' name="postalcode" required="">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-sm-12">
                                                    <fieldset class='float-label-field'>
                                                        <label for="txtcomment">Comments</label>
                                                        <input id="txtcomment" type='text' name="comments" >
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <br>
                                            <div style="text-align: right;">
                                                <input type="submit" class="btn submit-btn" value="Submit">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                    <!-----------------  Submit Design Modal End  -------------------->
                </div>

                <!--Code-->


            </div>
        </div>
</section>

<script src="{{ url('jquery.arctext.js') }}"></script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
    $(document).ready(function () {
        <?php for($i = 1;$i <= count($layers);$i++) { ?>
        $(".add-color-<?=$i?>").click(function () {
            <?php for($j = 1;$j <= count($layers);$j++) { if($j == $i) { ?>
            $("#myModal<?=$j?>").toggle();
            $("#myModal<?=$j?>").css("z-index", ("9999"));
            <?php } else{ ?>
            $("#myModal<?=$j?>").hide();
            <?php } } ?>
            $("#myModal-1").hide();
        });

        $("#close<?=$i?>").click(function () {
            $("#myModal<?=$i?>").css("display", "none");
        });
        <?php } ?>

        /* showing outline buttons for add design only. */
        /*	$(".add-design").click(function () {

                $(".outline-btns").show();

                 });
        */
        $('.css-accordion').on('click', 'li', function () {
            // snip...

            if ($(this).children().children("#addDesign").length > 0) {
                //alert($(this).children().children().html());
                $(".outline-btns").show();

            } else {

                //alert($(this).children().children().html());
                //		alert($(this).html());
                //		alert('not found');
                $(".outline-btns").hide();
            }
            //	var projIndex = $(this).index();
            //   console.log(projIndex)
            //  OpenProject()


        });


        $(".add-color--1").click(function () {
            $("#myModal-1").toggle();
            <?php for($i = 1;$i <= count($layers);$i++) { ?>
            $("#myModal<?=$j?>").hide();
            <?php } ?>
            $("#myModal-1").css("z-index", ("9999"));
        });

        $("#close-1").click(function () {
            $("#myModal-1").css("display", "none");
        });

        $(".m-show").click(function () {
            if ($(".m-show").hasClass("active-1")) {
            } else {
                $(".m-show").addClass("active-1");
                $(".s-hide").removeClass("active-1");
                $(".contact-box").show();
                $(".box-mrg").hide();
            }
        });

        $(".s-hide").click(function () {

            if ($(".s-hide").hasClass("active-1")) {
            } else {
                $(".s-hide").addClass("active-1");
                $(".m-show").removeClass("active-1");
                $(".contact-box").hide();
                $(".box-mrg").show();
            }
        });
    });

    $('.add-design').click(function () {
        console.log('Sufyan Design');
        $('.mymodal7').hide();
    });
    $('.add-color').click(function () {
        console.log('Sufyan color');
        $('.modal1').hide();
    });

    function close_logo() {
        console.log('Hello Sufyan');
        $('#myModal1-1-1').hide();
        $('#myModal1-2-2').hide();
        $('#myModal1-3-3').hide();
        $('#myModal1-4-4').hide();
        $('#myModal1-5-5').hide();
    }

    function addText(input, location, css) {
        var position = document.getElementById(location + 'Text');
        var fonts = $("#fonts option:selected").text();
        var sizes = $("#sizes option:selected").text();
        console.log(fonts);
        console.log(sizes);
        position.innerHTML = "<strong id='" + location + "_fonts_abc' style='font-family:" + fonts + ";  font-size:" + sizes + "'>" + input + "</strong>";
        document.getElementById(location + 'TextFile').value = input;
    }

    function text_style_change(style, parentDiv) {
        console.log('sufyan text_style_change');
        console.log(style);
        console.log(parentDiv);
        if (style == 'curve') {
            $('#' + parentDiv + '_fonts_abc').arctext({radius: 50, dir: -1});
            $('#' + parentDiv + 'TextFile').arctext({radius: 50, dir: -1});
        } else {
            var input = $('#' + parentDiv + '_text').val() ? $('#' + parentDiv + '_text').val() : $('#' + parentDiv + '_text1').val();
            addText(input, parentDiv, 'text-align:center;')
        }
    }

    function font_change(fonts, parentDiv) {
        console.log('sufyan fonts');
        console.log(fonts);
        console.log(parentDiv);
        $('#' + parentDiv + '_fonts_abc').css({'font-family': fonts});
        $('#' + parentDiv + 'TextFile').css({'font-family': fonts});
    }

    function size_change(sizes, parentDiv) {
        console.log('sufyan sizes');
        console.log(sizes);
        $('#' + parentDiv + '_fonts_abc').css({'font-size': sizes});
        $('#' + parentDiv + 'TextFile').css({'font-size': sizes});
    }

    function changetextColor(color, location, parentDiv) {
        console.log('Sufyan Text Color');
        $('#' + location).css({'color': color});
    }

    $(document).ready(function () {
        $(".btn-1").click(function () {
            console.log('asdsa');
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-5').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-1').css({'background-color': '#7bc143', 'color': 'white'});
            $(".all_alphabets").show();
            $(".one-one").hide();
            $(".two-one").hide();
            $(".three-one").hide();
            $(".four-one").hide();
            $(".five-one").hide();
            $(".six-one").hide();
            $(".one-one").css("z-index", ("9999"));
        });
        $(".btn-2").click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-5').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-2').css({'background-color': '#7bc143', 'color': 'white'});
            $(".one-one").hide();
            $(".two-one").show();
            $(".three-one").hide();
            $(".four-one").hide();
            $(".five-one").hide();
            $(".six-one").hide();
            $(".all_alphabets").hide();
            $(".two-one").css("z-index", ("9999"));
        });
        $(".btn-3").click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-5').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-3').css({'background-color': '#7bc143', 'color': 'white'});
            $(".one-one").hide();
            $(".two-one").hide();
            $(".three-one").show();
            $(".four-one").hide();
            $(".five-one").hide();
            $(".six-one").hide();
            $(".all_alphabets").hide();
            $(".three-one").css("z-index", ("9999"));
        });
        $(".btn-4").click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-5').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-4').css({'background-color': '#7bc143', 'color': 'white'});
            $(".one-one").hide();
            $(".two-one").hide();
            $(".three-one").hide();
            $(".four-one").show();
            $(".five-one").hide();
            $(".six-one").hide();
            $(".all_alphabets").hide();
            $(".four-one").css("z-index", ("9999"));
        });
        $(".btn-5").click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-5').css({'background-color': '#7bc143', 'color': 'white'});
            $(".one-one").show();
            $(".two-one").hide();
            $(".three-one").hide();
            $(".four-one").hide();
            $(".five-one").show();
            $(".six-one").hide();
            $(".all_alphabets").hide();
            $(".five-one").css("z-index", ("9999"));
        });
        $(".btn-6").click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-5').removeAttr("style");
            $('.btn-6').css({'background-color': '#7bc143', 'color': 'white'});
            $(".one-one").hide();
            $(".two-one").hide();
            $(".three-one").hide();
            $(".four-one").hide();
            $(".five-one").hide();
            $(".all_alphabets").hide();
            $(".six-one").show();
            $(".six-one").css("z-index", ("9999"));
        });
        $('#checkboxes').on('click', ':checkbox', function (e) {
            $('#checkboxes :checkbox').each(function () {
                if (this != e.target)
                    $(this).prop('checked', false);
            });
        });
        $('#one-my-design').click(function () {
            console.log('Sufyan Coloriing');
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-5').css({'background-color': '#7bc143', 'color': 'white'});
            $('#myModal1-2').hide();
            $('#myModal1-3').hide();
            $('#myModal1-4').hide();
            $('#myModal1-5').hide();
            $('#myModal1-1').show();
            $(".one-one").show();
            $("#myModal1-1").css("z-index", ("9999"));
        });
        $('#two-my-design').click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-5').css({'background-color': '#7bc143', 'color': 'white'});
            $('#myModal1-1').hide();
            $('#myModal1-3').hide();
            $('#myModal1-4').hide();
            $('#myModal1-5').hide();
            $('#myModal1-2').show();
            $(".one-one").show();
            $("#myModal1-2").css("z-index", ("9999"));
        });
        $('#three-my-design').click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-5').css({'background-color': '#7bc143', 'color': 'white'});
            $('#myModal1-1').hide();
            $('#myModal1-2').hide();
            $('#myModal1-4').hide();
            $('#myModal1-5').hide();
            $('#myModal1-3').show();
            $(".one-one").show();
            $("#myModal1-3").css("z-index", ("9999"));
        });
        $('#four-my-design').click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-5').css({'background-color': '#7bc143', 'color': 'white'});
            $('#myModal1-1').hide();
            $('#myModal1-2').hide();
            $('#myModal1-3').hide();
            $('#myModal1-5').hide();
            $('#myModal1-4').show();
            $(".one-one").show();
            $("#myModal1-4").css("z-index", ("9999"));
        });
        $('#five-my-design').click(function () {
            $('.btn-1').removeAttr("style");
            $('.btn-2').removeAttr("style");
            $('.btn-3').removeAttr("style");
            $('.btn-4').removeAttr("style");
            $('.btn-6').removeAttr("style");
            $('.btn-5').css({'background-color': '#7bc143', 'color': 'white'});
            $('#myModal1-1').hide();
            $('#myModal1-2').hide();
            $('#myModal1-3').hide();
            $('#myModal1-4').hide();
            $('#myModal1-5').show();
            $(".one-one").show();
            $("#myModal1-5").css("z-index", ("9999"));
        });


        jQuery('#close1-1').click(function () {
            console.log('Hello DJ');
            jQuery('#myModal1-1-1').hide();
            jQuery('#myModal1-1').hide();
            jQuery('#myModal1-1-1').hide();
            jQuery('#myModal1-2-2').hide();
            jQuery('#myModal1-3-3').hide();
            jQuery('#myModal1-4-4').hide();
            jQuery('#myModal1-5-5').hide();
        });
        jQuery('#close1-2').click(function () {
            jQuery('#myModal1-2').hide();
            jQuery('#myModal1-1-1').hide();
            jQuery('#myModal1-2-2').hide();
            jQuery('#myModal1-3-3').hide();
            jQuery('#myModal1-4-4').hide();
            jQuery('#myModal1-5-5').hide();
        });
        jQuery('#close1-3').click(function () {
            jQuery('#myModal1-3').hide();
            jQuery('#myModal1-1-1').hide();
            jQuery('#myModal1-2-2').hide();
            jQuery('#myModal1-3-3').hide();
            jQuery('#myModal1-4-4').hide();
            jQuery('#myModal1-5-5').hide();
        });
        jQuery('#close1-4').click(function () {
            jQuery('#myModal1-4').hide();
            jQuery('#myModal1-1-1').hide();
            jQuery('#myModal1-2-2').hide();
            jQuery('#myModal1-3-3').hide();
            jQuery('#myModal1-4-4').hide();
            jQuery('#myModal1-5-5').hide();
        });
        jQuery('#close1-5').click(function () {
            jQuery('#myModal1-5').hide();
            jQuery('#myModal1-1-1').hide();
            jQuery('#myModal1-2-2').hide();
            jQuery('#myModal1-3-3').hide();
            jQuery('#myModal1-4-4').hide();
            jQuery('#myModal1-5-5').hide();
        });
    });
    $('.stage-2').click(function () {
        $('#step-2').addClass("complete");//.css('background-image', 'url(../../images/StepBox-Green.png) !important')
    });
    $('.stage-3').click(function () {
        $('#step-3').addClass("complete");
        //$('.current-bg-3').css('background-image', 'url(../../images/StepBox-Green-End.png) !important')
    });
    $('#saveDesign').click(function (e) {
        console.log('Sufyan Form');
        $('#myModal-kit').show();
        var base = $('#base').val();
        var overlay = $('#overlay').val();
        var svgData = $('#svgData').val();

        var baseColor = $('#baseColor').val();
        var layer1Color = $('#layer1Color').val();
        var layer2Color = $('#layer2Color').val();
        var accentColor = $('#accentColor').val();
        var frontTRFile = $('#frontTRFile').val();
        var frontTLFile = $('#frontTLFile').val();
        var frontBRFile = $('#frontBRFile').val();
        var frontBLFile = $('#frontBLFile').val();
        var backCenterFile = $('#backCenterFile').val();
        var frontTRTextFile = $('#frontTRTextFile').val();
        var frontTRTextFilestyle = $('#frontTRTextFile').attr('style');
        var frontTLTextFile = $('#frontTLTextFile').val();
        var frontTLTextFilestyle = $('#frontTLTextFile').attr('style');
        var frontBRTextFile = $('#frontBRTextFile').val();
        var frontBRTextFilestyle = $('#frontBRTextFile').attr('style');
        var frontBLTextFile = $('#frontBLTextFile').val();
        var frontBLTextFilestyle = $('#frontBLTextFile').attr('style');
        var backCenterTextFile = $('#backCenterTextFile').val();
        var backCenterTextFilestyle = $('#backCenterTextFile').attr('style');
        var embs = $('#embs').val();


        //alert(svgDataStream);
        //alert(svgData);
        var svgDataStream = $('#svgDataDiv').html();

        $('#svgDataStream').val(svgDataStream);

        //console.log(svgDataStream);


        //console.log(base+" - "+overlay+" - "+svgData+" - "+baseColor+" - "+layer1Color+" - "+layer2Color+" - "+accentColor+" - "+frontTRFile+" - "+frontTLFile+" - "+frontBRFile+" - "+frontBLFile+" - "+backCenterFile);
        console.log(frontTRTextFile + " - " + frontTLTextFile + " - " + frontBRTextFile + " - " + frontBLTextFile + " - " + backCenterTextFile);
        console.log(frontTRTextFilestyle + " - " + frontTLTextFilestyle + " - " + frontBRTextFilestyle + " - " + frontBLTextFilestyle + " - " + backCenterTextFilestyle);
        $.ajax({
            type: 'POST',
            url: "{{ route('step1-addColor.download-pdf') }}",
            data: {
                base: base,
                overlay: overlay,
                svgData: svgData,
                overlay: overlay,
                baseColor: baseColor,
                layer1Color: layer1Color,
                layer2Color: layer2Color,
                accentColor: accentColor,
                frontTRFile: frontTRFile,
                frontTLFile: frontTLFile,
                frontBRFile: frontBRFile,
                frontBLFile: frontBLFile,
                backCenterFile: backCenterFile,
                frontTLTextFile: frontTLTextFile,
                frontTLTextFilestyle: frontTLTextFilestyle,
                frontTRTextFile: frontTRTextFile,
                frontTRTextFilestyle: frontTRTextFilestyle,
                frontBRTextFile: frontBRTextFile,
                frontBRTextFilestyle: frontBRTextFilestyle,
                frontBLTextFile: frontBLTextFile,
                frontBLTextFilestyle: frontBLTextFilestyle,
                backCenterTextFile: backCenterTextFile,
                backCenterTextFilestyle: backCenterTextFilestyle,
                embs: embs
            },
            success: function (result) {
                window.open(result);
                alert(result);
            }
        });
    });


    $('#saveDesign1').click(function (e) {
        $('#myModal-kit').show();
        var base = $('#base').val();
        var overlay = $('#overlay').val();
        var svgData = $('#svgData').val();
        var baseColor = $('#baseColor').val();
        var layer1Color = $('#layer1Color').val();
        var layer2Color = $('#layer2Color').val();
        var accentColor = $('#accentColor').val();
        var frontTRFile = $('#frontTRFile').val();
        var frontTLFile = $('#frontTLFile').val();
        var frontBRFile = $('#frontBRFile').val();
        var frontBLFile = $('#frontBLFile').val();
        var backCenterFile = $('#backCenterFile').val();

        //alert(svgDataStream);
        //alert(svgData);
        var svgDataStream = $('#svgDataDiv').html();

        $('#svgDataStream').val(svgDataStream);

        //console.log(svgDataStream);


        //console.log(base+" - "+overlay+" - "+svgData+" - "+baseColor+" - "+layer1Color+" - "+layer2Color+" - "+accentColor+" - "+frontTRFile+" - "+frontTLFile+" - "+frontBRFile+" - "+frontBLFile+" - "+backCenterFile);
        $.ajax({
            type: 'POST',
            url: "{{ route('step1-addColor.download-pdf') }}",
            data: {
                base: base,
                overlay: overlay,
                svgData: svgData,
                overlay: overlay,
                baseColor: baseColor,
                layer1Color: layer1Color,
                layer2Color: layer2Color,
                accentColor: accentColor,
                frontTRFile: frontTRFile,
                frontTLFile: frontTLFile,
                frontBRFile: frontBRFile,
                frontBLFile: frontBLFile,
                backCenterFile: backCenterFile
            },
            success: function (result) {
                window.open(result);
                alert(result);
            }
        });
    });


    $('.close-kit').click(function () {
        $('#myModal-kit').css('display', 'none');
    });
    $('.on-style').click(function () {
        $('.on-style').css('background', '#7bc143')
        $('.off-style').css('background', '#e9e5e5')
    });
    $('.off-style').click(function () {
        $('.off-style').css('background', '#7bc143')
        $('.on-style').css('background', '#e9e5e5')
    });
</script>
<!-- End Content -->
@include('layouts.front_footer')
</body>
<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        (function () {
            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');

            $('#custom1').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function (data, textStatus, jqXHR, pos) {
                    $('.image').val(jqXHR.responseText);
                    $('.image').hide();
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    var position = 'frontTR';//$('#position').val();
                    console.log(position);
                    addFrontTR("{{ asset('img/custom_img/') }}/" + jqXHR.responseText, 'frontTR');
                },
                complete: function (xhr) {
                }
            });
            $('#custom2').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function (data, textStatus, jqXHR, pos) {
                    $('.image').val(jqXHR.responseText);
                    $('.image').hide();
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    var position = 'frontTL';//$('#position').val();
                    console.log(position);
                    addFrontTL("{{ asset('img/custom_img/') }}/" + jqXHR.responseText, 'frontTL');
                },
                complete: function (xhr) {
                }
            });
            $('#custom3').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function (data, textStatus, jqXHR, pos) {
                    $('.image').val(jqXHR.responseText);
                    $('.image').hide();
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    var position = 'frontBR';//$('#position').val();
                    console.log(position);
                    addFrontBR("{{ asset('img/custom_img/') }}/" + jqXHR.responseText, 'frontBR');
                },
                complete: function (xhr) {
                }
            });
            $('#custom4').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function (data, textStatus, jqXHR, pos) {
                    $('.image').val(jqXHR.responseText);
                    $('.image').hide();
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    var position = 'frontBL';//$('#position').val();
                    console.log(position);
                    addFrontBL("{{ asset('img/custom_img/') }}/" + jqXHR.responseText, 'frontBL');
                },
                complete: function (xhr) {
                }
            });
            $('#custom5').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function (data, textStatus, jqXHR, pos) {
                    $('.image').val(jqXHR.responseText);
                    $('.image').hide();
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    var position = 'backCenter';//$('#position').val();
                    console.log(position);
                    addBackCenter("{{ asset('img/custom_img/') }}/" + jqXHR.responseText, 'backCenter');
                },
                complete: function (xhr) {
                }
            });
        })();
    });
    getDefaultColors();
    //addOutline();
    removeOutline();
    <?php
    if(isset($_REQUEST['base'])) {
    ?>
    changeBaseColor('#<?=$_REQUEST['base']?>');
    <?php
    }
    if(isset($_REQUEST['layer1'])) {
    ?>
    changeLayer1Color('#<?=$_REQUEST['layer1']?>');
    <?php
    }
    if(isset($_REQUEST['layer2'])) {
    ?>
    changeLayer2Color('#<?=$_REQUEST['layer2']?>');
    <?php
    }
    if(isset($_REQUEST['layer3'])) {
    ?>
    changeLayer3Color('#<?=$_REQUEST['layer3']?>');
    <?php
    }
    ?>
    /*var mySVG = document.getElementsByTagName('svg');

    for(i=0; i<mySVG.length;i++)
        mySVG[i].setAttribute("viewBox", "0 0 1200 1200");*/
</script>
<script>
    var closemodal = document.getElementById('myModal2');
    var closemodal2 = document.getElementById('myModal3');
    var closemodal3 = document.getElementById('myModal4');
    var closemodal4 = document.getElementById('myModal5');
    var closemodal10 = document.getElementById('myModal6');
    var closemodal5 = document.getElementById('myModal1-1');
    var closemodal6 = document.getElementById('myModal1-2');
    var closemodal7 = document.getElementById('myModal1-3');
    var closemodal8 = document.getElementById('myModal1-4');
    var closemodal9 = document.getElementById('myModal1-5');

    window.onclick = function (event) {
        if (event.target == closemodal) {
            closemodal.style.display = "none";
        }
        if (event.target == closemodal2) {
            closemodal2.style.display = "none";
        }
        if (event.target == closemodal3) {
            closemodal3.style.display = "none";
        }
        if (event.target == closemodal4) {
            closemodal4.style.display = "none";
        }
        if (event.target == closemodal5) {
            closemodal5.style.display = "none";
        }
        if (event.target == closemodal6) {
            closemodal6.style.display = "none";
        }
        if (event.target == closemodal7) {
            closemodal7.style.display = "none";
        }
        if (event.target == closemodal8) {
            closemodal8.style.display = "none";
        }
        if (event.target == closemodal9) {
            closemodal9.style.display = "none";
        }
        if (event.target == closemodal10) {
            closemodal10.style.display = "none";
        }
    }
</script>
<script>
    var $li = $('stage-2').click(function () {
        $li.removeClass('stage-2');
        $(this).addClass('current-step-1');
    });
</script>
<script>
    $('input[type="checkbox"]').on('change', function () {
        $('input[type="checkbox"]').not(this).prop('checked', false);
    });
</script>
<script>
    var allStates = $("svg > *");

    allStates.on("click", function () {
        var id = this.getAttribute('id');
        if (id == 'l-1') {
            $("#myModal1").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal1").css("z-index", ("9999"));
        } else if (id == 'l-2') {
            $("#myModal2").show();
            $("#myModal1").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal2").css("z-index", ("9999"));
        } else if (id == 'l-3') {
            $("#myModal3").show();
            $("#myModal2").hide();
            $("#myModal1").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal3").css("z-index", ("9999"));
        } else if (id == 'l-4') {
            $("#myModal4").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal1").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal4").css("z-index", ("9999"));
        } else if (id == 'l-5') {
            $("#myModal5").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal1").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal5").css("z-index", ("9999"));
        } else if (id == 'l-6') {
            $("#myModal6").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal1").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal6").css("z-index", ("9999"));
        } else if (id == 'l-7') {
            $("#myModal7").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal1").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal7").css("z-index", ("9999"));
        } else if (id == 'l-8') {
            $("#myModal8").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal1").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal8").css("z-index", ("9999"));
        } else if (id == 'l-9') {
            $("#myModal9").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal1").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal9").css("z-index", ("9999"));
        } else if (id == 'l-10') {
            $("#myModal10").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal1").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal10").css("z-index", ("9999"));
        } else if (id == 'l-11') {
            $("#myModal11").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal1").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal11").css("z-index", ("9999"));
        } else if (id == 'l-12') {
            $("#myModal12").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal1").hide();
            $("#myModal-1").hide();
            $("#myModal12").css("z-index", ("9999"));
        }

        allStates.removeClass("on");
        $(this).addClass("on");
    });
</script>
<script>
    $(function () {
        $('#step-1').addClass('current-step-1');
    });
</script>
<script>
    <?php
    if($embs != '')
    {
    $f = asset("img/embellishments/") . "/" . $embs[0]->file;
    ?>
    applyEmbStep1('{!! $f !!}');
    <?php
    }
    ?>
</script>
<script src="{{asset('js/download.js')}}"></script>
<!----Model Script--->
<script src="{{asset('js/model.js')}}"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
crossorigin="anonymous"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js"></script> -->
<script>
    $('head').append('<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">');

    $('input').focus(function (event) {
        $(this).closest('.float-label-field').addClass('float').addClass('focus');
    })

    $('input').blur(function () {
        $(this).closest('.float-label-field').removeClass('focus');
        if (!$(this).val()) {
            $(this).closest('.float-label-field').removeClass('float');
        }
    });
</script>
<script>
    var svgDataDiv = document.getElementById('svgDataDiv');
    console.log("Number of Layers: " + svgDataDiv.getElementsByTagName('path').length);
    for (var i = 0; i < svgDataDiv.getElementsByTagName('path').length; i++) {
        //<li class="add-color-1"><span>&#9675</span>Body</li>
    }
</script>
<script>
    $( document ).ready(function() {
        $(".symbol-none").click(function(){
            $(".selected-img-block svg#Layer_1").remove();
        });
    });
</script>
<script>
    $( document ).ready(function() {
        event.preventDefault();
        $("button.rmve-col").click(function(){
            $('path#layer-1').css({ fill: "#000000" });
            window.location.reload(true);
        });
    });
</script>
<script>
    $( document ).ready(function() {
        $("button.rmve-col").click(function(){
            $('path#layer-3').css({ fill: "#7d193a" });
            window.location.reload(true);
        });
    });
</script>
<script>
    $( document ).ready(function() {
        $("button.rmve-col").click(function(){
            $('path#layer-2').css({ fill: "#727272" });
            window.location.reload(true);
        });
    });
</script>
<script>
    $( document ).ready(function() {
        $("button.rmve-col").click(function(){
            $('path#layer-4').css({ fill: "#df3174" });
            window.location.reload(true);
        });
    });
</script>
<script>
    $( document ).ready(function() {
        $(".companies-logo").click(function(){
            $(".selected-img-holder svg#Layer_1").remove();
        });
    });
</script>

@include('layouts.front_footerScript')
</html>